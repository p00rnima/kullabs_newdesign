<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
<!--                <li class="active">Exposure</li>-->
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="courses_box1">
                    <div class="col-md-6 about_left">
                        <h1>Welcome</h1>
                        <p>What does it take to become a knowledgeable person? Knowledge and experience.
                            Kul Techno Lab and Research Center is creating a global platform that will help every individual in this world to share their knowledge and experience with the use of technology. Technology has become a part of our life and yet we cannot use it to implore the ideas to make a good use of it in education.
                        </p>
                        <p>We are a group of young individuals who have different backgrounds but share a common idea of changing the education system of the world by creating a new revolutionary educational system that will use knowledge, experience and technology to teach according to the need of the learner and become a backbone of the education system of the new age.</p>
                        <h4>Our vision:</h4>
                        <ul class="about_links">
                            <li>Provide Free, variety and best contents for e-learning.</li>
                            <li>Create a platform where every component of education merges.</li>
                            <li>Provide multiple resources for teachers and students to teach and learn.</li>
                            <li>Provide freedom to the users to learn whatever they want, wherever they are and however they want.</li>
                            <li>Share and forward ideas to enhance the learning experience.</li>
                        </ul>
                        <a href="#" class="radial_but">Read More</a>
                    </div>
                    <div class="col-md-6 _aboutimages">
                        <img src="images/6.jpg" class="img-responsive" alt="">
                    </div>
                    <div class="clearfix"> </div>
                </div>
            <div class="staff_list">
                    <h1>KulLabs Team</h1>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 pull-right-md"> <!-- required for floating -->
                            <div class="side-bar">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-right">
                                    <li class="active"><a href="#profile-r" data-toggle="tab">Sushil Dev Bhattarai ( Co-founder, Chairperson and CEO)</a></li>
                                    <li><a href="#messages-r" data-toggle="tab">Adhip Poudyal (Co-founder, Vice-chairperson and COO)</a></li>
                                    <li><a href="#settings-r1" data-toggle="tab">Shuvaraj Sharma (Head Research And Academics )</a></li>
                                    <li><a href="#settings-r" data-toggle="tab">Sudil Joshi </a></li>
                                    <li><a href="#settings-r3" data-toggle="tab">Adarsha Chaulagain ( Director, Chief Marketing &amp; Publicity Officer) </a></li>
                                    <li><a href="#settings-r4" data-toggle="tab">Kumar Magar (ShareHolder)</a></li>

                                    <li><a href="#settings-r5" data-toggle="tab">Rajesh Magar (ShareHolder)</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- team member -->
                                <div class="tab-pane active" id="profile-r">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Sushil Dev Bhattraib ( Co-founder, Chairperson and CEO)</h4>
                                            <p>All of us had our Engineering completed and were in dilemma, whether to stay in Nepal and do something or go abroad like most of the people do nowadays. None of us wanted to leave our motherland but we found nothing that could stick us here. When we found a dead end with least opportunities and chaos competition, it was then when all of us decided to do something that had never been done before. We began calculating the necessities of the society, we chose the education industry at first. We began our approach to government and private schools in the Kathmandu valley. We were astonished, nothing had changed inside the schools for a very long time, same way of teaching and learning as we and our fathers did. The world was going crazy using latest computer technologies outside the school premises' but inside the school we felt an irritating environment. There was no charm and passion in both teachers and students while teaching and learning, instantly we found out that there was something we could revolutionize, Why not empower students, teachers and schools with all the resources they need on their fingertips, so that teaching and learning would become much more interactive and efficient. </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- team member -->
                                <div class="tab-pane" id="messages-r">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Adhip Poudyal (Co-founder, Vice-chairperson and COO)</h4>
                                            <p>All of us had our Engineering completed and were in dilemma, whether to stay in Nepal and do something or go abroad like most of the people do nowadays. None of us wanted to leave our motherland but we found nothing that could stick us here. When we found a dead end with least opportunities and chaos competition, it was then when all of us decided to do something that had never been done before. We began calculating the necessities of the society, we chose the education industry at first. We began our approach to government and private schools in the Kathmandu valley. We were astonished, nothing had changed inside the schools for a very long time, same way of teaching and learning as we and our fathers did. The world was going crazy using latest computer technologies outside the school premises' but inside the school we felt an irritating environment. There was no charm and passion in both teachers and students while teaching and learning, instantly we found out that there was something we could revolutionize, Why not empower students, teachers and schools with all the resources they need on their fingertips, so that teaching and learning would become much more interactive and efficient. </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- team member -->
                                <!-- team member -->
                                <div class="tab-pane" id="profile-r">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Sushil Dev Bhattarai ( Co-founder, Chairperson and CEO)</h4>
                                            <p>With a zeal resolution of changing the education system of Nepal Sushil Dev Bhattarai, one of the cofounders of Kullabs started the project. Best on what he does he thought of the revolutionary idea of combining the education with technology. He leads the team with his understanding expertise and sharehis vision like a visionary. He functions as the brain and visionary of the company. </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- team member -->
                                <div class="tab-pane" id="messages-r">
                                    <div class="vertical-text">
                                        <!--   <div class="member-img"> <img src="images/img1.jpg" alt="">
                                            <div class="img-detail">
                                              <p>Loremipsum Dummy Text3</p>
                                            </div>
                                          </div> -->
                                        <div class="well content-text">
                                            <h4>Adhip Poudyal (Co-founder, Vice-chairperson and COO)</h4>
                                            <p>The next co-founder of this institution, he is a young vibrant individual with a hard passion for his work. With computer engineering as his background he develops complex logics and ideas that helps move forward the vision of Kullabs.  Strong determinant and excellent management skills makes him an excellent Chief Operating Officer.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- team member -->
                                <div class="tab-pane" id="settings-r">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Sudil Joshi ( Assistant Operating Officer)</h4>
                                            <p>An artistic vision with engineering background makes Sudil Joshi an exceptional Assistant Operating Officer of our team. Unique conceptualizing ideas along with polishing skills makes him unique in our team. Though he comes from engineering background his marketing advices makes him a good entrepreneur.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="settings-r1">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Shuvaraj Sharma (Head Research And Academics )</h4>
                                            <p>A hardcore reseacher and knowledeable personality who has almost everything in the universe in his brains.He creates criteria for content development through our valued reserach team.</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="settings-r3">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Adarsha Chaulagain</h4>
                                            <p>A Young and enthusiastic MBA graduate who wishes to utilize his expertise in the field of education and technology. His knowledge is utilized in the overall management of the organization.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="settings-r4">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class="well content-text">
                                            <h4>Kumar Magar</h4>
                                            <p>A young business personality born in Neapal with his business based in China who wants to gift his country with tech-friendly and free education. His experience in business has become fruitful for the company.</p>
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="settings-r5">
                                    <div class="vertical-text">
                                        <!-- <div class="member-img"> <img src="images/img1.jpg" alt="">
                                          <div class="img-detail">
                                            <p>Loremipsum Dummy Text3</p>
                                          </div>
                                        </div> -->
                                        <div class=" well content-text">
                                            <h4>Rajesh Magar</h4>
                                            <p>A father who kept searching for better educational contents for his child got a right path when he found about kulllabs.com. He dreams of a society where no child has to be physically ad mentally tortured with heavy bags of books.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
<!--        --><?php //include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>