<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row">

       <div class="col-md-9">

                <div class="content-text">
                    <h4>units and measurement</h4>

                    <p>
                        Upon completion of this lesson, students should be able to:
                    </p>
                    <ul>
                        <li>Define units and know the importance of measurement.Define units and know the importance of measurement.Define units and know the importance of measurement.Define units and know the importance of measurement.Define units and know the importance of measurement.Define units and know the importance of measurement.</li>
                        <li>Define various system of measurements.fine units and know the importance of measurement.</li>
                        <li>Solve simple numerical problems related to measurement.</li>
                    </ul>
                </div> <!-- end content-text text -->

                <div class="notes">
                    <h4>notes</h4>

                    <div class="media note-content">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="images/studentpage/measurement.png" alt="Measurement">
                            </a>
                        </div>

                        <div class="media-body">
                            <h4 class="media-heading">Measurement</h4>

                            <!-- breadcrumbs -->
                            <ul class="breadcrumb breadcrumb-link">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active"><a href="#">More</a></li>
                            </ul>

                            <p>The process of comparing an unknown physical quantities with known standard quantities is called measurement. This note contain information about measurement and different quantities related to measurement.</p>

                            <div class="links">
                                <ul>
                                    <li>
                                        <a href="#" class="sub-link exercise-link">
                                            <i class="fa fa-question fa_icons" aria-hidden="true"></i>
                                            Exercises
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="sub-link video-link">
                                            <i class="fa fa-video-camera fa_icons" aria-hidden="true"></i>
                                            Watch Video
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="sub-link test-link">
                                            <i class="fa fa-pencil fa_icons" aria-hidden="true"></i>
                                            Practise Test
                                        </a>
                                    </li>
                                </ul>
                            </div> <!-- end links -->
                        </div> <!-- end media-body -->
                    </div> <!-- end media div -->

                    <div class="media note-content">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="images/studentpage/measurement.png" alt="Measurement">
                            </a>
                        </div>

                        <div class="media-body">
                            <h4 class="media-heading">Mass</h4>

                            <!-- breadcrumbs -->
                            <ul class="breadcrumb breadcrumb-link">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active"><a href="#">More</a></li>
                            </ul>

                            <p>The process of comparing an unknown physical quantities with known standard quantities is called measurement. This note contain information about measurement and different quantities related to measurement.</p>

                            <div class="links">
                                <ul>
                                    <li>
                                        <a href="#" class="sub-link exercise-link">
                                            <i class="fa fa-question fa_icons" aria-hidden="true"></i>
                                            Exercises
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="sub-link video-link">
                                            <i class="fa fa-video-camera fa_icons" aria-hidden="true"></i>
                                            Watch Video
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="sub-link test-link">
                                            <i class="fa fa-pencil fa_icons" aria-hidden="true"></i>
                                            Practise Test
                                        </a>
                                    </li>
                                </ul>
                            </div> <!-- end links -->
                        </div> <!-- end media-body -->
                    </div> <!-- end media div -->

                    <div class="media note-content">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="images/studentpage/measurement.png" alt="Measurement">
                            </a>
                        </div>

                        <div class="media-body">
                            <h4 class="media-heading">Measurement Of Time</h4>

                            <!-- breadcrumbs -->
                            <ul class="breadcrumb breadcrumb-link">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li class="active"><a href="#">More</a></li>
                            </ul>

                            <p>The process of comparing an unknown physical quantities with known standard quantities is called measurement. This note contain information about measurement and different quantities related to measurement.</p>

                            <div class="links">
                                <ul>
                                    <li>
                                        <a href="#" class="sub-link exercise-link">
                                            <i class="fa fa-question fa_icons" aria-hidden="true"></i>
                                            Exercises
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="sub-link video-link">
                                            <i class="fa fa-video-camera fa_icons" aria-hidden="true"></i>
                                            Watch Video
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="sub-link test-link">
                                            <i class="fa fa-pencil fa_icons" aria-hidden="true"></i>
                                            Practise Test
                                        </a>
                                    </li>
                                </ul>
                            </div> <!-- end links -->
                        </div> <!-- end media-body -->
                    </div> <!-- end media div -->

                </div> <!-- end notes div -->

                <!-- Pagination -->
                <div class="page-counter">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div> <!-- end page counter div -->

            </div>

        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>