<!-- Side-Nav-->
<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <!--  <div class="user-panel">
          <div class="pull-left image"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image" class="img-circle"></div>
          <div class="pull-left info">
            <p>John Doe</p>
            <p class="designation">Frontend Developer</p>
          </div>
          </div> -->
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">
            <h2 class="_head1">Profile</h2>
            <li class="active"><a href="page-user.php"><i class="fa fa-edit"></i><span>My Timeline</span></a></li>
            <li><a href="page-info.php"><i class="fa fa-info-circle"></i><span>Info</span></a></li>
            <hr class="_horzline">
            <h2 class="_head1">E-learning</h2>
            <li class="treeview">
                <a href="#"><i class="fa fa-book"></i><span>Class 7</span><i class="fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="bootstrap-componants.html"><i class="fa fa-circle-o"></i> Bootstrap Elements</a></li>
                    <li><a href="ui-font-awesome.html"><i class="fa fa-circle-o"></i> Font Icons</a></li>
                    <li><a href="ui-cards.html"><i class="fa fa-circle-o"></i> Cards</a></li>
                    <li><a href="widgets.html"><i class="fa fa-circle-o"></i> Widgets</a></li>
                </ul>
            </li>
            <li><a href="charts.html"><i class="fa fa-book"></i><span>Grade Lists</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-book"></i><span>Class 8</span><i class="fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Form Componants</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Custom Componants</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Form Samples</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Form Notifications</a></li>
                </ul>
            </li>
            <hr class="_horzline">
            <h2 class="_head1">Games</h2>
            <li><a href=""><i class="fa fa-info-circle"></i><span>Quiz</span></a></li>
            <li><a href=""><i class="fa fa-info-circle"></i><span>The Hangman</span></a></li>
            <li><a href=""><i class="fa fa-info-circle"></i><span>ABCD</span></a></li>
        </ul>
    </section>
</aside>