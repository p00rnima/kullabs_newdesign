<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.html" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-9 c-container-div" style="background-color:#fff;margin-top: 10px;">
            <h2 class="heading-topic">GRAVITY</h2>
            <div id="c-tabs">
                <ul>
                    <li class="c-active" data-id="c-tab-note">Note</li>
                    <li data-id="c-tab-thing">Things to remember</li>
                    <li data-id="c-tab-videos">Videos</li>
                    <li data-id="c-tab-exercise">Exercise</li>
                    <li data-id="c-tab-quiz">Quiz</li>
                </ul>
            </div>
            <div>
                <div id="c-tab-note">
                    <div class="container-content">

                        <h3 class="heading-second-topic">INTRODUCTION TO GRAVITY</h3>
                        <p>The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.</p>
                        <h3 class="heading-second-topic">GRAVITATIONAL FIELD</h3>
                        <p>The force of attraction between any two bodies in the universe is called gravitation. This note had information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.</p>
                        <h3 class="heading-second-topic">ACCELERATION DUE TO GRAVITY</h3>
                        <p>It is the acceleration produced on a freely falling object towards the center at any planet of satelite.</p>
                    </div>
                    <div class="box-pre">
                        <p style="left: 0"><img src="images/e-learning/left-arrow.png"><label>Force</label></p>
                        <p style="right:0;"><label>Field</label>
                            <img src="images/e-learning/right-arrow.png"></p>
                    </div>
                    <h2 class="heading-topic">Any Questions 'Gravity'?</h2>
                    <textarea class="form-control"></textarea>
                    <button class="c-button">Submit Question</button><br><br>
                    <h2 class="heading-topic">DISCUSSIONS ABOUT THIS NOTE</h2>
<!--                    <div class="comment-container">-->
<!--                        <div class="container-user-comment">-->
<!--                            <div class="media">-->
<!--                                <div class="media-left">-->
<!--                                    <a href="#">-->
<!--                                        <img class="media-object" src="images/home/student.png">-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4 class="media-heading">Aayush Shrestha <span class="comment-time">2 months ago</span></h4>-->
<!--                                    <p>This is my askdf asdjf. This is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjfThis is my askdf asdjf</p>-->
<!--                                    <ul>-->
<!--                                        <li><a href="">0 Reply</a></li>-->
<!--                                        <li><a href="">Reply</a></li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="container-user-comment">-->
<!--                            <div class="media">-->
<!--                                <div class="media-left">-->
<!--                                    <a href="#">-->
<!--                                        <img class="media-object" src="images/home/student.png">-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4 class="media-heading">Aayush Shrestha <span class="comment-time">2 months ago</span></h4>-->
<!--                                    <p>This is my comment.</p>-->
<!--                                    <ul>-->
<!--                                        <li><a href="">0 Reply</a></li>-->
<!--                                        <li><a href="">Reply</a></li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <table class="table table-bordered table-hover">
                        <thead style="background-color:#4267b2">
                        <tr>
                            <th>Forum</th>
                            <th>Time</th>
                            <th>Replies</th>
                            <th>Report</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td style="width: 50%;">
                                <h5><i class="fa fa-user"></i> Anju Gurung</h5>
                                <p>Define Gravity.</p>
                            </td>
                            <td class="forum-link" style="width: 25%;">
                                <p><small>Feb 05, 2017</small></p>
<!--                                <a class="note-comment-reply note-comment-reply-1998" data-id="1998" style="cursor: pointer;">Reply</a>-->
                            </td>
                            <td class="forum-link" style="width: 25%;">
                                <p class="count-replies-1998">0 Replies</p>
                                <a style="cursor: pointer;" class="viewAllReplies">View Replies</a>
                            </td>
                            <td> <span style="cursor: pointer;" title="Report this comment" class="pull-right report-comment-button report-comment-button-1998" data-id="1998" data-toggle="modal" data-target="#reportModal"><i class="fa fa-warning"></i></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">
                                <h5><i class="fa fa-user"></i> yanjeen Rumpa</h5>
                                <p>why effect of gravitation is seen more in liquid than in solid?</p>
                            </td>
                            <td class="forum-link" style="width: 25%;">
                                <p><small>Feb 01, 2017</small></p>
<!--                                <a class="note-comment-reply note-comment-reply-1770" data-id="1770" style="cursor: pointer;">Reply</a>-->
                            </td>
                            <td class="forum-link" style="width: 25%;">
                                <p class="count-replies-1770">0 Replies</p>
                                <a style="cursor: pointer;" class="viewAllReplies">View Replies</a>
                            </td>
                            <td> <span style="cursor: pointer;" title="Report this comment" class="pull-right report-comment-button report-comment-button-1770" data-id="1770" data-toggle="modal" data-target="#reportModal"><i class="fa fa-warning"></i></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="c-tab-thing" class="c-container-hidden">
                    <div class="container-content">
                        <!-- <h2 class="heading-topic">GRAVITY</h2>
                          <h3 class="heading-second-topic">INTRODUCTION TO GRAVITY</h3> -->
                        <p>The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.</p>
                        <!--  <h3 class="heading-second-topic">GRAVITATIONAL FIELD</h3>
                          <p>The force of attraction between any two bodies in the universe is called gravitation. This note had information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.</p>
                          <h3 class="heading-second-topic">ACCELERATION DUE TO GRAVITY</h3>
                          <p>It is the acceleration produced on a freely falling object towards the center at any planet of satelite.</p> -->
                    </div>
                    <div class="box-pre">
                        <p style="left: 0">
                            <img src="images/e-learning/left-arrow.png">
                            <label>Force</label></p>
                        <p style="right:0;"><label>Field</label>
                            <img src="images/e-learning/right-arrow.png">
                        </p>
                    </div>
                    <h2 class="heading-topic">Any Questions 'Gravity'?</h2>
                    <textarea class="form-control"></textarea>
                    <button class="c-button">Submit Question</button><br><br>
                    <h2 class="heading-topic">DISCUSSIONS ABOUT THIS NOTE</h2>
                </div>
                <div id="c-tab-videos" class="c-container-hidden">
                    <div class="container-content">
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumbnail video-gallery">
                                <a href=""><img class="img-responsive md6image" src=" http://img.youtube.com/vi/cafXa6Z-elA/0.jpg" width="100%"></a>
                                <div class="caption">
                                    <p><a href="video.html">Social Development</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumbnail video-gallery">
                                <a href=""><img class="img-responsive md6image" src=" http://img.youtube.com/vi/fvRHhlrumOg/0.jpg" width="100%"></a>
                                <div class="caption">
                                    <p><a href="">Social Development</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-xs"></div>
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumbnail video-gallery">
                                <a href=""><img class="img-responsive md6image" src=" http://img.youtube.com/vi/K-bDogGDBx8/0.jpg" width="100%"></a>
                                <div class="caption">
                                    <p><a href="">Social Development</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-sm"></div>
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumbnail video-gallery">
                                <a href=""><img class="img-responsive md6image" src=" http://img.youtube.com/vi/lnUSXdmHrVQ/0.jpg" width="100%"></a>
                                <div class="caption">
                                    <p><a href="">Social Development</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-xs"></div>
                        <div class="clearfix visible-md visible-lg"></div>
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumbnail video-gallery">
                                <a href=""><img class="img-responsive md6image" src=" http://img.youtube.com/vi/lnUSXdmHrVQ/0.jpg" width="100%"></a>
                                <div class="caption">
                                    <p><a href="">Nepal Wrieless Networking Project [ Himalayan Waves ]</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="c-tab-exercise" class="c-container-hidden">
                    <!-- accordion -->
                    <div class="questions" style="margin-top:-21px;">
                        <h3>Very Short Questions</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title exercise">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            1)  Define development in your own sentence.?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title exercise">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2) Define development in your own sentence.
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title exercise">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3)   Define development in your own sentence.
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="c-tab-quiz" class="c-container-hidden">
                    <div role="tabpanel" class="tab-pane active" id="quiz" data-loaded="1">
                        <div class="progress" id="practice-test-progress" style="margin-bottom: -1px;">
                            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" id="practice_test_progress" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%; min-width: 2em;">
                                0%
                            </div>
                        </div>
                        <div class="anythingSlider anythingSlider-default activeSlider" style="width: 555px; height: 320px;">
                            <div class="anythingWindow">
                                <ul id="practice_test" class="anythingBase horizontal c-anythingBase">
                                    <li>
                                        <h3>1.) What is the total percentage of people who speak Maithili language according to 2011 census?</h3>
                                        <div id=''><input type='radio' class='obj_answers' name='' value=''> <span id=''>about 10%</span><br><input type='radio' class='obj_answers' name='' value=''> <span id=''>about 12%</span><br><input type='radio' class='obj_answers' name='' value=''> <span id=''>about 8%</span><br><input type='radio' class='obj_answers' name='' value=''> <span id=''>about 9%</span><br></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right-Sidebar-->
        <?php include_once('right-sidebar.php') ?>
    </div>

</div>
</div>



<!-- Javascripts-->
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/plugins.js"></script>
<script src="js/exposureslider.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.anythingslider.js"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $("#c-tabs ul li").on('click', function(){
            var dataId = $(this).attr("data-id");
            removeClass();
            $(this).addClass("c-active");
            $("#" + dataId).show();
            console.log(dataId);
        });
    });
    function removeClass(){
        $("#c-tabs ul li").removeClass("c-active");
        $("#c-tab-note").hide();
        $("#c-tab-thing").hide();
        $("#c-tab-videos").hide();
        $("#c-tab-exercise").hide();
        $("#c-tab-quiz").hide();
    }
</script>
</body>
</html>