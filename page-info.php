<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('user-sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row user">
        <div class="col-md-9 c-container-div" style="margin-top: 10px;margin-left:-13px;">
            <div class="card user-settings">
                <h4 class="line-head">Info</h4>
                <!-- start FORM -->
                <br style="clear:both">
                <a class="pull-left" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="showWorkForm">
                    <i class="fa fa-plus add-title" aria-hidden="true"></i> Add Work </a>
                <br style="clear:both">
                <!-- start WORK collapse form -->
                <div class="collapse" id="showWorkForm">
                    <div class="col-md-12">
                        <!--<div class="form-area"  id="current-pane">-->
                        <div class="form-area">
                            <form role="form" style="margin-bottom: 10%" id="collapseOnClose" class="collapse in">
                                <!--<a class="pull-right" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                <br style="clear:both">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Company Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Position" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="City" required="">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" id="message" placeholder="Description" maxlength="140" rows="3"></textarea>
                                    <!--<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>-->
                                </div>
                                <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                <button type="button" id="submit" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="showWorkForm"> Close </a>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end WORK collapse form -->
                <hr class="" style="clear:both">

                <!-- start COLLEGE -->
                <a class="pull-left" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="showCollegeForm">
                    <i class="fa fa-plus add-title" aria-hidden="true"></i>
                    Add College
                </a>
                <br style="clear:both">

                <div class="collapse" id="showCollegeForm">
                    <!-- start collapse form -->
                    <div class="col-md-12">
                        <!--<div class="form-area"  id="current-pane">-->
                        <div class="form-area">
                            <form role="form" id="collapseOnClose2" class="form-margin collapse in">
                                <!--<a class="pull-right" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                <br style="clear:both">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Company Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" placeholder="Position" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="mobile" placeholder="City" required="">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" placeholder="Description" maxlength="140" rows="3"></textarea>
                                    <!--<span class="help-block"><p class="help-block ">You have reached the limit</p></span>-->
                                </div>
                                <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                <button type="button" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="showCollegeForm"> Close </a>
                            </form>
                        </div>
                    </div> <!-- end collapse form -->
                </div>
                <!-- end COLLEGE -->

                <hr class="hr-collapse-form" style="clear:both">
                <h3 class="profile-title" style=""> <i class="title-icon fa fa-briefcase" aria-hidden="true"></i> KulLabs Pvt. Ltd.</h3>
                <hr>
                <h3 class="profile-title"> <i class="title-icon fa fa-university" aria-hidden="true"></i> Islington College</h3>
                <hr>
                <h3> <i class="title-icon fa fa-briefcase" aria-hidden="true"></i> <span class="profile-title"> KulLabs Pvt. Ltd.</span> <a class="pull-right collapsed" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="showWorkForm"> Edit Your Work</a></h3>

                <!-- start edit WORK collapse form -->
                <div class="collapse" id="editWorkForm" aria-expanded="false" style="height: 0px;">
                    <div style="padding:19px;">
                        <!--<div class="form-area"  id="current-pane">-->
                        <div class="form-area">
                            <form role="form" id="collapseOnClose3" class="form-margin collapse in">
                                <!--<a class="pull-right" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                <br style="clear:both">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Company Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" placeholder="Position" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="mobile" placeholder="City" required="">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" placeholder="Description" maxlength="140" rows="3"></textarea>
                                    <!--<span class="help-block"><p class="help-block ">You have reached the limit</p></span>-->
                                </div>
                                <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                <button type="button" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                <a class="btn btn-default pull-right collapsed" role="button" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="editWorkForm"> Close </a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>