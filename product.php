<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
                <li class="active">Exposure</li>
                <li class="active">Exposure</li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 card">
                <!-- product -->
                <div class="col-md-2 col-sm-4">

                </div>

                <!-- product -->
                <div class="col-md-2 col-sm-4">
                    <div class="product-block">
                        <div class="product-img">
                            <img src="images/science8.jpg" alt="">
                        </div>

                        <div class="product-detail">
                            <h5>Classs 10 Science</h5>
                            <p>Price NRs. 650</p>

                            <div class="download">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <p>Details</p>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h5 class="modal-title" id="myModalLabel">Kullabs SmartSchool Software for Class 8 science (NRs. 750.00)</h5>
                                            </div>

                                            <!-- modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-4">
                                                        <div class="demo-img">
                                                            <a href="#">
                                                                <img src="images/science8.jpg" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 col-sm-8">
                                                        <div class="features">
                                                            <p>This educational software has following features: </p>
                                                            <ul>
                                                                <li><i>88</i> Notes</li>
                                                                <li><i>250+</i> Vdeos</li>
                                                                <li><i>800+</i> Solved Exercises</li>
                                                                <li><i>380+</i> Quizzes</li>
                                                            </ul>
                                                        </div>

                                                        <div class="features">
                                                            <p>To try this software, download our demo* for this software.</p>
                                                            <ul>
                                                                <li>File <i>social10Demo.exe</i></li>
                                                                <li>Size <i>20.57 MB+</i></li>
                                                            </ul>
                                                        </div>

                                                        <button class="btn btn-log" type="button">Download Demo</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <p>*This free demo gives you access to the note, exercises, proactice session and videos for the first topic of first lesson.</p>
                                                <p>To purchase the full version of software, give us a call at 01-4419284 during office hours.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- product -->
                <div class="col-md-2 col-sm-4">
                    <div class="product-block">
                        <div class="product-img">
                            <img src="images/science8.jpg" alt="">
                        </div>

                        <div class="product-detail">
                            <h5>Classs 10 Science</h5>
                            <p>Price NRs. 650</p>

                            <div class="download">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <p>Details</p>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h5 class="modal-title" id="myModalLabel">Kullabs SmartSchool Software for Class 8 science (NRs. 750.00)</h5>
                                            </div>

                                            <!-- modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-4">
                                                        <div class="demo-img">
                                                            <a href="#">
                                                                <img src="images/science8.jpg" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 col-sm-8">
                                                        <div class="features">
                                                            <p>This educational software has following features: </p>
                                                            <ul>
                                                                <li><i>88</i> Notes</li>
                                                                <li><i>250+</i> Vdeos</li>
                                                                <li><i>800+</i> Solved Exercises</li>
                                                                <li><i>380+</i> Quizzes</li>
                                                            </ul>
                                                        </div>

                                                        <div class="features">
                                                            <p>To try this software, download our demo* for this software.</p>
                                                            <ul>
                                                                <li>File <i>social10Demo.exe</i></li>
                                                                <li>Size <i>20.57 MB+</i></li>
                                                            </ul>
                                                        </div>

                                                        <button class="btn btn-log" type="button">Download Demo</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <p>*This free demo gives you access to the note, exercises, proactice session and videos for the first topic of first lesson.</p>
                                                <p>To purchase the full version of software, give us a call at 01-4419284 during office hours.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- product -->
                <div class="col-md-2 col-sm-4">
                    <div class="product-block">
                        <div class="product-img">
                            <img src="images/science8.jpg" alt="">
                        </div>

                        <div class="product-detail">
                            <h5>Classs 10 Science</h5>
                            <p>Price NRs. 650</p>

                            <div class="download">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <p>Details</p>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h5 class="modal-title" id="myModalLabel">Kullabs SmartSchool Software for Class 8 science (NRs. 750.00)</h5>
                                            </div>

                                            <!-- modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-4">
                                                        <div class="demo-img">
                                                            <a href="#">
                                                                <img src="images/science8.jpg" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 col-sm-8">
                                                        <div class="features">
                                                            <p>This educational software has following features: </p>
                                                            <ul>
                                                                <li><i>88</i> Notes</li>
                                                                <li><i>250+</i> Vdeos</li>
                                                                <li><i>800+</i> Solved Exercises</li>
                                                                <li><i>380+</i> Quizzes</li>
                                                            </ul>
                                                        </div>

                                                        <div class="features">
                                                            <p>To try this software, download our demo* for this software.</p>
                                                            <ul>
                                                                <li>File <i>social10Demo.exe</i></li>
                                                                <li>Size <i>20.57 MB+</i></li>
                                                            </ul>
                                                        </div>

                                                        <button class="btn btn-log" type="button">Download Demo</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <p>*This free demo gives you access to the note, exercises, proactice session and videos for the first topic of first lesson.</p>
                                                <p>To purchase the full version of software, give us a call at 01-4419284 during office hours.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- product -->
                <div class="col-md-2 col-sm-4">
                    <div class="product-block">
                        <div class="product-img">
                            <img src="images/science8.jpg" alt="">
                        </div>

                        <div class="product-detail">
                            <h5>Classs 10 Science</h5>
                            <p>Price NRs. 650</p>

                            <div class="download">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <p>Details</p>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h5 class="modal-title" id="myModalLabel">Kullabs SmartSchool Software for Class 8 science (NRs. 750.00)</h5>
                                            </div>

                                            <!-- modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-4">
                                                        <div class="demo-img">
                                                            <a href="#">
                                                                <img src="images/science8.jpg" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 col-sm-8">
                                                        <div class="features">
                                                            <p>This educational software has following features: </p>
                                                            <ul>
                                                                <li><i>88</i> Notes</li>
                                                                <li><i>250+</i> Vdeos</li>
                                                                <li><i>800+</i> Solved Exercises</li>
                                                                <li><i>380+</i> Quizzes</li>
                                                            </ul>
                                                        </div>

                                                        <div class="features">
                                                            <p>To try this software, download our demo* for this software.</p>
                                                            <ul>
                                                                <li>File <i>social10Demo.exe</i></li>
                                                                <li>Size <i>20.57 MB+</i></li>
                                                            </ul>
                                                        </div>

                                                        <button class="btn btn-log" type="button">Download Demo</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <p>*This free demo gives you access to the note, exercises, proactice session and videos for the first topic of first lesson.</p>
                                                <p>To purchase the full version of software, give us a call at 01-4419284 during office hours.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-4">
                    <div class="product-block">
                        <div class="product-img">
                            <img src="images/science8.jpg" alt="">
                        </div>

                        <div class="product-detail">
                            <h5>Classs 10 Science</h5>
                            <p>Price NRs. 650</p>

                            <div class="download">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                                    <p>Details</p>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h5 class="modal-title" id="myModalLabel">Kullabs SmartSchool Software for Class 8 science (NRs. 750.00)</h5>
                                            </div>

                                            <!-- modal body -->
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-4">
                                                        <div class="demo-img">
                                                            <a href="#">
                                                                <img src="images/science8.jpg" class="img-responsive" alt="">
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 col-sm-8">
                                                        <div class="features">
                                                            <p>This educational software has following features: </p>
                                                            <ul>
                                                                <li><i>88</i> Notes</li>
                                                                <li><i>250+</i> Vdeos</li>
                                                                <li><i>800+</i> Solved Exercises</li>
                                                                <li><i>380+</i> Quizzes</li>
                                                            </ul>
                                                        </div>

                                                        <div class="features">
                                                            <p>To try this software, download our demo* for this software.</p>
                                                            <ul>
                                                                <li>File <i>social10Demo.exe</i></li>
                                                                <li>Size <i>20.57 MB+</i></li>
                                                            </ul>
                                                        </div>

                                                        <button class="btn btn-log" type="button">Download Demo</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <p>*This free demo gives you access to the note, exercises, proactice session and videos for the first topic of first lesson.</p>
                                                <p>To purchase the full version of software, give us a call at 01-4419284 during office hours.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-more pull-right">View More <span class="fa fa-arrow-down"></span></button>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>