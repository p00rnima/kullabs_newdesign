<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="card" style="margin-top: 6px;">
                <div class="blog-item" style="margin-top:15px; text-align: justify;margin-right: 20px">
                    <div class="row">
                        <div class="blog-content">
                            <div class="media">

                                <div class="media-body blog-detail">
                                    <h2 class="media-heading">सबसंग ज्ञान बटुल्न पर्छ</h2>

                                    <p style="text-align: justify;">एक दिन एक महात्मालाई उनको साथिले भनेछन, “तिमीसंग ज्ञान लिन दुनियाभरबाट विद्वानहरु आउँछन्, र तिम्रो कुरा सुनेर आफ्नो जीवन धन्य भएको सम्झिन्छन्, तर मैले आजसम्म तिम्रो एउटा कुरा चाँही राम्ररी बुझेको छैन। यो सुनेर महात्मा भन्छन्, “मेरो कुन कुरामा तिमीलाई संका लाग्यो, त्यो त भन।“</p>
                                    <p style="text-align: justify;">ती साथिले भने, “तिमी त आफै ठुलो विद्वान र ज्ञानी छौ, तर मैले देखेको छु कि हरक्षण तिमि अरुबाट शिक्षा लिन तत्पर रहन्छौ। त्यो पनि निकै ठुलो उत्साह र उमंग साथ। यो भन्दा ठुलो कुरा त तिमीलाई साधारण व्यक्तिहरुबाट सिक्न पनि केहि अप्ठ्यारो हुदैन। तिमीलाई आखिर सिख्नको के जरुरी? कि तिनीहरुलाई खुशी गर्न उनीहरुबाट सिकेको नाटक मात्र गर्छौ?</p>
                                    <p style="text-align: justify;">यो कुरा सुनेर ती महात्मा जोर जोर संग हास्न लागे। ती साथीले के भयो भनेर सोध्दा महात्मा भन्छन्, “मानिस आफ्नो पुरै जीवनमा कुनै कुरा पूर्ण रुपमा सिक्न सक्दैन, जहिले पनि केहि न केहि अपुरो रहन्छ नै। र हरेक मानिससंग जहिले पनि केहि न केहि यस्तो हुन्छ नै जुन अरुसंग हुदैन। त्यसैले हरेक मानिससंग सिकिराख्न पर्छ। र फेरी हर कुरा र अनुभव किताबहरुमा त हुँदैन, किनकि धेरै यस्ता कुरा हुन्छन् जुन लेखिएको हुँदैन। वास्तविकतामा बसेर अरु मानिसहरुसंग सिक्ने बानि बनाए तिमी पुरा ज्ञान त पाउँदैनौ, तर पूर्णताको नजिकै अवश्य पुग्नेछौ। यो नै जीवनको सार हो।“</p>



                                </div>
                            </div>
                        </div>
                        <div class="bottom-article">
                            <ul class="meta-post">
                                <li><i class="icon-calendar"></i>2017-02-09 09:21:31</li>
                                <li><i class="icon-user"></i>Kullabs Admin</li>
                                <!--  <li><i class="icon-folder-open"></i><a href="#"> Blog</a></li> -->
                                <li><i class="icon-comments"></i><a href="#">0 comments</a></li>
                                <li class="fb-share-button fb_iframe_widget" data-href="https://kullabs.com/blogs/blogDetail/78/10" data-layout="button_count" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=1565311873683656&amp;container_width=11&amp;href=https%3A%2F%2Fkullabs.com%2Fblogs%2FblogDetail%2F78%2F10&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey"><span style="vertical-align: bottom; width: 68px; height: 20px;"><iframe name="f30a54e1a5cdba8" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:share_button Facebook Social Plugin" src="https://www.facebook.com/v2.8/plugins/share_button.php?app_id=1565311873683656&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2Fao6eUeuGXQq.js%3Fversion%3D42%23cb%3Df3377e7a4b4da98%26domain%3Dkullabs.com%26origin%3Dhttps%253A%252F%252Fkullabs.com%252Ff189451e139686%26relation%3Dparent.parent&amp;container_width=11&amp;href=https%3A%2F%2Fkullabs.com%2Fblogs%2FblogDetail%2F78%2F10&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey" style="border: none; visibility: visible; width: 68px; height: 20px;" class=""></iframe></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>