<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Video detail page</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-9">
            <h2 class="heading-topic">Video Title</h2>
            <div class="bs-example" data-example-id="responsive-embed-16by9-iframe-youtube">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="550" height="250" src="https://www.youtube.com/embed/sinQ06YzbJI" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <p>This one is video description. Write your video description here.The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force. The force of attraction between any two bodies in the universe is called gravitation. This note has information about universal law of gravitation, its effect and variables that affect gravitational force.</p>
            <h2 class="heading-topic">Other Related Videos</h2>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="thumbnail video-gallery">
                        <a href="https://www.kullabs.com/notes/video/play/g_6gWmKwpjk/13048">
                            <img class="img-responsive md6image" src=" https://img.youtube.com/vi/g_6gWmKwpjk/0.jpg" width="100%"></a>
                        <div class="caption">
                            <p><a href="https://www.kullabs.com/notes/video/play/g_6gWmKwpjk/13048">Elements of Society</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="thumbnail video-gallery">
                        <a href="https://www.kullabs.com/notes/video/play/lo78k4NOVJE/13052">
                            <img class="img-responsive md6image" src=" https://img.youtube.com/vi/lo78k4NOVJE/0.jpg" width="100%">
                        </a>
                        <div class="caption">
                            <p><a href="https://www.kullabs.com/notes/video/play/lo78k4NOVJE/13052">Elements of society</a></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-xs visible-sm"></div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="thumbnail video-gallery">
                        <a href="https://www.kullabs.com/notes/video/play/Pwja1TmMe7Y/13050">
                            <img class="img-responsive md6image" src=" https://img.youtube.com/vi/Pwja1TmMe7Y/0.jpg" width="100%">
                        </a>
                        <div class="caption">
                            <p><a href="https://www.kullabs.com/notes/video/play/Pwja1TmMe7Y/13050">Elements of good society</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="thumbnail video-gallery">
                        <a href="https://www.kullabs.com/notes/video/play/rHSrgjCciWI/13054">
                            <img class="img-responsive md6image" src=" https://img.youtube.com/vi/rHSrgjCciWI/0.jpg" width="100%">
                        </a>
                        <div class="caption">
                            <p><a href="https://www.kullabs.com/notes/video/play/rHSrgjCciWI/13054">Elements of good society</a></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-md visible-lg"></div>
                <div class="clearfix visible-xs visible-sm"></div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <div class="thumbnail video-gallery">
                        <a href="https://www.kullabs.com/notes/video/play/yOzm8yH4_6I/13058">
                            <img class="img-responsive md6image" src=" https://img.youtube.com/vi/yOzm8yH4_6I/0.jpg" width="100%">
                        </a>
                        <div class="caption">
                            <p><a href="https://www.kullabs.com/notes/video/play/yOzm8yH4_6I/13058">Elements of Society</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>