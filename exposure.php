<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<!-- content -->
      <div class="content-wrapper">
          <div class="page-title">
              <!--  <div>
                <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
                <p> admin template</p>
                </div> -->
              <div>
                  <ul class="breadcrumb">
                      <li><a href="#">Home</a></li>
                      <li><a href="#">About us</a></li>
                      <li class="active">Exposure</li>
                  </ul>
              </div>
              <div>
                  <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>
              </div>
          </div>

        <div class="exposure-detail">
            <div class="col-md-9">
                 <div class="exposure-list">
      
                    <div class="customNavigation">
                      <a class="btn prev"><i class="fa fa-angle-left"></i></a>
                      <a class="btn next"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <div id="owl-demo" class="" ss="owl-carousel">
                      <div class="detail">
                        <div class = "caption category_title" data-id="category01">
                            <p class="category-active">upload notes</p>
                        </div>
                       </div>
                       <div class="detail">
                        <div class ="caption category_title" data-id="category02">
                            <p>Videos</p>
                        </div>
                      </div>
                      <div class="detail">
                        <div class ="caption category_title" data-id="category03">
                            <p>Music</p>
                        </div>
                      </div>
                      <div class="detail">
                        <div class ="caption category_title" data-id="category04">
                            <p>Blog</p>
                        </div>
                      </div>
                      <div class="detail">
                        <div class ="caption category_title" data-id="category05">
                            <p>Free E-Books</p>
                        </div>
                      </div>
                      <div class="detail">
                        <div class ="caption category_title" data-id="category06">
                            <p>Forum</p>
                        </div>
                      </div>

                      <div class="detail">
                        <div class ="caption category_title" data-id="category07">
                            <p>Advertisment</p>
                        </div>
                      </div>
                      <div class="detail">
                        <div class ="caption category_title" data-id="category08">
                            <p>Free E-Books</p>
                        </div>
                      </div>
                      
                </div>
            </div>
                <div class="well exposure-subdetail">
                    <div class="category_user" id="category01">
                        <h3><img src="http://kullabs.com/img/home/Choice.png">
                            <a href="http://kullabs.com/blogs/blogDetail/18/3">Anju Gurung</a>
                        </h3>
                        <div class="media">
                            <div class="media-left categorylist">
                                <a href="#">
                                    <img class="media-object category-img" src="images/40.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h2 class="media-heading">मरुभूमि हुदै नेपाल
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" data-placement="top" data-toggle="tooltip" title="Edit" >
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                        <button class="btn btn-danger btn-xs" data-placement="top" data-toggle="tooltip" title="Delete" >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </div>
                                </h2>
                                <h4 class="media-heading">सुन्दा अचम्म लाग्ने कुरा हो, तर कल्पना गर्नुहोस्, ग्लोबल वार्मिंगले बढ्दो तापमान र जलवायु परिवर्तनले नेपाल मरुभूमिमा परिणत हुन
                                    सक्ने अवस्था सिर्जना भइराखेको छ। तपाईंले नेपालमा गर्मी महिनामा धेरै गर्मि महसुस गरेको हुनुहुन्छ नै तर ज
                                    ाडो महिनामा आजकल दिनभर न्यानो पनि हुने गर्छ? मनसुन पहिलेजस्त्तै समयमा आइपुग्ने गर्दैन र पहिले...</h4>
                                <div class="breadcrump">
                                    <a href="">Blogs</a>
                                    <a href="">Interesting Facts</a>
                                </div>
                                <a href="">
                                    <img src="images/fb.png" width="79px">
                                </a>
                                <a href="exposure_detail.php"><button class="read_more">Read More</button></a>
                            </div>
                        </div>
                        <hr>
                        <h3><img src="http://kullabs.com/img/home/Choice.png">
                            <a href="http://kullabs.com/blogs/blogDetail/18/3">Shuva Sharma</a></h3>
                        <div class="media">
                            <div class="media-left categorylist">
                                <a href="#">
                                    <img class="media-object category-img" src="images/40.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h2 class="media-heading">मरुभूमि हुदै नेपाल
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" data-placement="top" data-toggle="tooltip" title="Edit" >
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                        <button class="btn btn-danger btn-xs" data-placement="top" data-toggle="tooltip" title="Delete" >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </div>
                                </h2>
                                <h4 class="media-heading">सुन्दा अचम्म लाग्ने कुरा हो, तर कल्पना गर्नुहोस्, ग्लोबल वार्मिंगले बढ्दो तापमान र जलवायु परिवर्तनले नेपाल मरुभूमिमा परिणत हुन
                                    सक्ने अवस्था सिर्जना भइराखेको छ। तपाईंले नेपालमा गर्मी महिनामा धेरै गर्मि महसुस गरेको हुनुहुन्छ नै तर ज
                                    ाडो महिनामा आजकल दिनभर न्यानो पनि हुने गर्छ? मनसुन पहिलेजस्त्तै समयमा आइपुग्ने गर्दैन र पहिले...</h4>
                                <div class="breadcrump">
                                    <a href="">Blogs</a>
                                    <a href="">Interesting Facts</a>
                                </div>
                                <a href="">
                                    <img src="images/fb.png" width="79px">
                                </a>
                                <a href="exposure_detail.php"><button class="read_more">Read More</button></a>
                            </div>
                        </div>
                         <hr>
                    </div>

                    <div class="category_user" id="category02" class="custom-hidden">
                        <h3><img src="http://kullabs.com/img/home/Choice.png">
                            <a href=""></a>Anju Gurung</h3>
                        <div class="media">
                            <div class="media-left categorylist">
                                <a href="#">

                                        <iframe width="200" height="113" src="https://www.youtube.com/embed/FNPwe93moXo" frameborder="0" allowfullscreen=""></iframe>

                                </a>
                            </div>
                            <div class="media-body">
                                <h2 class="media-heading">मरुभूमि हुदै नेपाल
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-xs" data-placement="top" data-toggle="tooltip" title="Edit" >
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </button>
                                    <button class="btn btn-danger btn-xs" data-placement="top" data-toggle="tooltip" title="Delete" >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </div>
                                </h2>
                                <h4 class="media-heading">सुन्दा अचम्म लाग्ने कुरा हो, तर कल्पना गर्नुहोस्, ग्लोबल वार्मिंगले बढ्दो तापमान र जलवायु परिवर्तनले नेपाल मरुभूमिमा परिणत हुन
                                    सक्ने अवस्था सिर्जना भइराखेको छ। तपाईंले नेपालमा गर्मी महिनामा धेरै गर्मि महसुस गरेको हुनुहुन्छ नै तर ज
                                    ाडो महिनामा आजकल दिनभर न्यानो पनि हुने गर्छ? मनसुन पहिलेजस्त्तै समयमा आइपुग्ने गर्दैन र पहिले...</h4>
                                <div class="breadcrump">
                                    <a href="">Blogs</a>
                                    <a href="">Interesting Facts</a>
                                </div>
                                <a href="">
                                    <img src="images/fb.png" width="79px">
                                </a>
                                <a href="http://kullabs.com/blogs/blogDetail/18/3"><button class="read_more">Read More</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="category_user" id="category03" class="custom-hidden">

                    </div>
                    <div class="category_user" id="category04" class="custom-hidden">

                    </div>
                    <div class="category_user" id="category05" class="custom-hidden">

                    </div>
                    <div class="category_user" id="category06" class="custom-hidden">

                    </div>
                    <div class="category_user" id="category07" class="custom-hidden">

                    </div>
                    <div class="category_user" id="category08" class="custom-hidden">

                    </div>


                </div>


            </div>

            <?php include_once('right-sidebar.php') ?>
        </div> <!-- END of lesson-page -->
      </div>
    </div>
    <!-- Javascripts-->
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/essential-plugins.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/exposureslider.js"></script>
   
    <script type="text/javascript">        
      $('#theCarousel').carousel({
      interval: false
      })
      
      $('.multi-item-carousel .item').each(function(){
      var next = $(this).next();
      if (!next.length) {
      next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      
      if (next.next().length>0) {
      next.next().children(':first-child').clone().appendTo($(this));
      }
      else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
      $(".owl-item").css("width","110px !important");
      });
</script>
<script>
    $(function(){
        $(".category_title").on('click', function(){
            var dataId = $(this).attr("data-id");
            console.log(dataId);
            hideAll();
            $(this).addClass("category-active");
            $("#"+dataId).show();
        });
        function hideAll(){
            $(".category_title p").removeClass("category-active");
            $("#category01").hide();
            $("#category02").hide();
            $("#category03").hide();
            $("#category04").hide();
            $("#category05").hide();
        }
    });
</script>

  </body>
</html>