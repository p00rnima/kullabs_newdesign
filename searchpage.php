<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/search.css">
    <link rel="stylesheet" type="text/css" href="css/search-responsive.css">
    <link href="css/anythingSlider.css" rel="stylesheet">
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <title>Kullabs Smart School</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
      script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
      script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
      -->
</head>
<body class="sidebar-mini fixed" style="background-color:#fff;">
    <div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print">
        <a href="index.php" class="logo"><img src="images/logo.png" style="margin-top:-15px;"></a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <!-- Navbar Right Menu-->
            <ul class="nav navbar-nav navbar-left" style="margin:0 0 0 1em">
                <div class="search">
                    <form>
                        <input type="text" value="" placeholder="search...">
                        <input type="submit" value="">
                    </form>
                </div>
            </ul>
            <div class="navbar-custom-menu navbar-right">
                <ul class="top-nav">
                    <!--Notification Menu-->
                    <li class="dropdown notification-menu">
                        <a href="#" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">E-learning</a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li>
                                <a href="javascript:;" class="media">
                                    <span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="media">
                                    <span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="media">
                                    <span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div>
                                </a>
                            </li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                    <li class=" notification-menu">
                        <a href="#">Revenue</a>
                    </li>
                    <li class=" notification-menu">
                        <a href="#">Exposure</a>
                    </li>
                    <li class=" notification-menu">
                        <a href="#">Anti-Drug Campaign</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <body>
    <div class="container search-div">
     <div class="row">
        <div class="col-md-12">
            <div class="tabbable-line">
                <ul class="nav nav-tabs ">
                    <li class="active">
                        <a href="#tab_notes_1" data-toggle="tab">
                            Notes</a>
                    </li>
                    <li>
                        <a href="#tab_videos_2" data-toggle="tab">
                            Videos </a>
                    </li>
                    <li>
                        <a href="#tab_exercises_3" data-toggle="tab">
                            Exercises</a>
                    </li>
                    <li>
                        <a href="#tab_images_4" data-toggle="tab">
                            Images</a>
                    </li>
                    <li>
                        <a href="#tab_news_5" data-toggle="tab">
                            News </a>
                    </li>
                    <li>
                        <a href="#tab_MCQ_6" data-toggle="tab">
                            MCQ </a>
                    </li>
                    <li>
                        <a href="#tab_schools_7" data-toggle="tab">
                            Schools </a>
                    </li>
                    <li>
                        <a href="#tab_users_8" data-toggle="tab">
                            Users </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_notes_1">
                        <h4>Notes Matching '<em>gravity</em>'</h4>
                        <p style="margin: 0px">Showing 1 to 10 of 271 entries</p>
<!--                        <div class="result">-->
<!--                            <a href="" target="_blank"><h4>Science</h4></a>-->
<!--                            <a href="" class="result_url">science/physics/gravity/gravitional force</a>-->
<!--                            <p class="snippet">Contact Site. Menu. Menu. English. English. English. Menu Learn · beta. IE Sucks-->
<!--                                IE is a Moron | the Official Extension for Chrome. Download Now.Duis autem eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.-->
<!--                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>-->
<!--                        </div>-->
<!--                        <div class="result">-->
<!--                            <a href="" target="_blank"><h4>Science</h4></a>-->
<!--                            <a href="" class="result_url">science/physics/gravity/gravitional force</a>-->
<!--                            <p class="snippet">Contact Site. Menu. Menu. English. English. English. Menu Learn · beta. IE Sucks-->
<!--                                IE is a Moron | the Official Extension for Chrome. Download Now.Duis autem eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.-->
<!--                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>-->
<!--                        </div>-->
                        <div class="media _search">
                            <div class="g">
                                <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Gravity</a></h4>
                                    <a href="">Grade 10</a> »<a href=""> Science</a> » <a href="">Physics</a>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="g">
                                <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Gravity</a></h4>
                                    <a href="">Grade 10</a> »<a href=""> Science</a> » <a href="">Physics</a>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_videos_2">
                        <div class="media search_video">
                            <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                            <div class="media-left">
                                <a href="#">
                                    <iframe width="120" height="90" src="https://www.youtube.com/embed/EcmzKbJsWtw" frameborder="0" allowfullscreen></iframe>
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" style="text-align:justify">
                                    Gravity</h4>
                                <a href="">Grade 10</a> »<a href=""> Science</a> » <a href="">Physics</a>
                                <p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                    In this note, you will learn about gravity along with its mathematical formulas.</p>
                            </div>
                        </div>
                        <div class="media search_video">
                            <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                            <div class="media-left">
                                <a href="#">
                                    <iframe width="120" height="90" src="https://www.youtube.com/embed/EcmzKbJsWtw" frameborder="0" allowfullscreen></iframe>
                                </a>

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" style="text-align:justify">
                                    Gravity</h4>
                                <a href="">Grade 10</a> »<a href=""> Science</a> » <a href="">Physics</a>
                                <p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                    In this note, you will learn about gravity along with its mathematical formulas.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_exercises_3">
                        <!-- accordion -->
                        <div class="accordion">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                What is measurement?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body panel-exercise">
                                            <p>The process of comparing an unknown physical quantity with known standard quantity of the same kind is called measurement.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                What is measurement?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body panel-exercise">
                                            <p>The process of comparing an unknown physical quantity with known standard quantity of the same kind is called measurement.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_images_4">
                        <div class="main">
                            <ul id="og-grid" class="og-grid">
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/2.jpg" data-title="Veggies sunt bona vobis" data-description="Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.">
                                        <img src="images/thumbs/2.jpg" alt="img02"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/3.jpg" data-title="Dandelion horseradish" data-description="Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.">
                                        <img src="images/thumbs/3.jpg" alt="img03"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
                                        <img src="images/thumbs/1.jpg" alt="img01"/>
                                    </a>
                                </li>
                            </ul>
                            <p>Filler text by <a href="http://veggieipsum.com/">Veggie Ipsum</a></p>
                            <a id="og-additems" href="#">add more</a>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_news_5">
                        <div class="media _search">
                            <div class="g">
                                <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Gravity</a></h4>
                                    <div class="slp"><span class="_tQb _IId">Science Daily</span><span class="_v5">-</span><span class="f nsa _uQb">Jan 13, 2017</span></div>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="g">
                                <a href=""><h4>Top 10 Science Experiments - Experiments You Can Do at Home ...</h4></a>
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Gravity</a></h4>
                                    <div class="slp"><span class="_tQb _IId">Science Daily</span><span class="_v5">-</span><span class="f nsa _uQb">Jan 13, 2017</span></div>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_MCQ_6">
                        <div class="result" style="margin-top:-35px;">
                            <a href="" target="_blank"><h4>Science</h4></a>
                            <a href="" class="result_url">science/physics/gravity/gravitional force</a>
                            <ul class="_listing">
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned?</a></li>
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned?</a></li>
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned?</a></li>
                            </ul>
                        </div>
                        <div class="result">
                            <a href="" target="_blank"><h4>Science</h4></a>
                            <a href="" class="result_url">science/physics/gravity/gravitional force</a>
                            <ul class="_listing">
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned.</a></li>
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned.</a></li>
                                <li><a href="">Discuss your Project details with your Mentor and Group members as per the theme or subject assigned.</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="tab-pane" id="tab_schools_7">
                        <div class="media _search">
                            <div class="g">
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/Choice.png">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Kullabs Smart School</a></h4>
                                    <div class="slp"><span class="_tQb _IId">Putalisadak,kathmandu,Nepal</span><span class="_v5">-</span><span class="f nsa _uQb"><a href="" style="color:blue">https://kullabs.com/</a></span></div>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="g">
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/kul-logo.png">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Kullabs Smart School</a></h4>
                                    <div class="slp"><span class="_tQb _IId">Putalisadak,kathmandu,Nepal</span><span class="_v5">-</span><span class="f nsa _uQb"><a href="" style="color:blue">https://kullabs.com/</a></span></div>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_users_8">
                        <div class="media _search">
                            <div class="g">
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/female.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Anju Gurung</a></h4>
                                    <div class="slp"><span class="_tQb _IId">New Summit College</span><span class="_v5">-</span><span class="f nsa _uQb">BSc.CSIT</span></div>
                                    <div class="st">
                                        <p>Jorpati,kathmandu</p>
                                        <p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="g">
                                <div class="media-left _images">
                                    <a href="#">
                                        <img src="images/1.jpg">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading _title" style="text-align:justify">
                                        <a href=""> Rabin Shrestha</a></h4>
                                    <div class="slp"><span class="_tQb _IId">Oxford College </span><span class="_v5">-</span><span class="f nsa _uQb">class:12</span></div>
                                    <div class="st"><p>Force due to which a relatively small object is attracted towards the center of the larger objects like planets or satellites is known as gravity.
                                            In this note, you will learn about gravity along with its mathematical formulas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/grid.js"></script>
    <script>
        $(function() {
            Grid.init();
        });
    </script>
<script src="js/main.js"></script>
</body>
</html>