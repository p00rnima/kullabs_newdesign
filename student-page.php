<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
        <div>
            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>
        </div>
    </div>
    <div class="row">
        <div class="note_div">
            <div class="col-md-9">
                <div class="col-md-4">
                    <!--begin tabs going in wide content -->
                    <ul class="nav nav-tabs" id="maincontent" role="tablist">
                        <li class="active">
                            <a href="#RecentNotes" role="tab" data-toggle="tab">
                                <span class="note_count">12</span>
                                <div class="recent_note">Recent Notes</div>
                            </a>
                        </li>
                        <li>
                            <a href="#RecentVideos" role="tab" data-toggle="tab">
                                <span class="note_count">3</span>
                                <div class="recent_note">Recent post</div>
                            </a>
                        </li>
                    </ul>
                    <!--/.nav-tabs.content-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="RecentNotes">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td><a href="" class="notice_option">Basic for UX Designer</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option">10 steps to improve your wireframe</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option">A Better Way To Request App Ratings</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option">How To Speed Up Your WordPress Website</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right" <="" span=""></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                        <div class="tab-pane fade" id="RecentVideos">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td><a href="" class="notice_option">Basic for UX Designer</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option">10 steps to improve your wireframe</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right" <="" span=""></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                    </div>
                    <!--/.tab-content -->
                </div>
                <div class="col-md-4">
                    <!--begin tabs going in wide content -->
                    <ul class="nav nav-tabs" id="maincontent" role="tablist">
                        <li class="active">
                            <a href="#FactsNews" role="tab" data-toggle="tab">
                                <span class="note_count01">27</span>
                                <div class="recent_note01">Facts/News</div>
                            </a>
                        </li>
                        <li>
                            <a href="#Events" role="tab" data-toggle="tab">
                                <span class="note_count01">3</span>
                                <div class="recent_note01">Events</div>
                            </a>
                        </li>
                    </ul>
                    <!--/.nav-tabs.content-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="FactsNews">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td><a href="" class="notice_option"><span class="fa fa-comment-o"></span> How to make good logo?</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option"><span class="fa fa-comment-o"></span> What are the best mobile apps for ...</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option"><span class="fa fa-comment-o"></span> What's the difference between UI and ...</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="notice_option"><span class="fa fa-comment-o"></span> What logos include hidden messages ...</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right" <="" span=""></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                        <div class="tab-pane fade" id="Events">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td><a href="" class="notice_option">Basic for UX Designer</a></td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right" <="" span=""></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                    </div>
                    <!--/.tab-content -->
                </div>
                <div class="col-md-4">
                    <!--begin tabs going in wide content -->
                    <ul class="nav nav-tabs" id="maincontent" role="tablist">
                        <li class="active">
                            <a href="#UserActivity" role="tab" data-toggle="tab">
                                <span class="note_count02">12</span>
                                <div class="recent_note02">User Activity</div>
                            </a>
                        </li>
                        <li>
                            <a href="#DoneExams" role="tab" data-toggle="tab">
                                <span class="note_count02">3</span>
                                <div class="recent_note02">Done Exams</div>
                            </a>
                        </li>
                    </ul>
                    <!--/.nav-tabs.content-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="UserActivity">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 70%;">
                                                        <span class="percent_only">70%</span>
                                                    </div>
                                                </div>
                                                Junior web designer
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 90%;">
                                                        <span class="percent_only">90%</span>
                                                    </div>
                                                </div>
                                                Junior web designer of viral
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 50%;">
                                                        <span class="percent_only">50%</span>
                                                    </div>
                                                </div>
                                                What are great examples of viral
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 50%;">
                                                        <span class="percent_only">50%</span>
                                                    </div>
                                                </div>
                                                What are great examples of viral
                                                ..
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right"></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                        <div class="tab-pane fade" id="DoneExams">
                            <div class="content table-responsive">
                                <table class="table table-hover ">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 70%;">
                                                        <span class="percent_only">70%</span>
                                                    </div>
                                                </div>
                                                Junior web designer
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 90%;">
                                                        <span class="percent_only">90%</span>
                                                    </div>
                                                </div>
                                                What are great examples of viral ..
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 50%;">
                                                        <span class="percent_only">50%</span>
                                                    </div>
                                                </div>
                                                Which is the best way to pass the PMP ...
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="" class="notice_option">
                                                <div class="progress progress-bar-vertical">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 50%;">
                                                        <span class="percent_only">50%</span>
                                                    </div>
                                                </div>
                                                Why is consistency important in design?
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="" class="more_option">Show more <span class="fa  fa-angle-double-right"></span></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/.tab-pane -->
                    </div>
                    <!--/.tab-content -->
                </div>
                <div class="form_discuss">
                    <!-- Default panel contents -->
                    <div class="forum_title"><h4>Forum/Discussion</h4></div>
                    <!-- List group -->
                    <ul class="list-group">
                        <li class="list-group-item">

                            <span class="image"><img src="images/studentpage/man.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a>  film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>


                        </li>
                        <li class="list-group-item">

                            <span class="image"><img src="images/studentpage/man1.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a>  film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>

                        </li>
                        <li class="list-group-item">

                            <span class="image"><img src="images/studentpage/girl.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a>  film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>

                        </li>
                        <li class="list-group-item">
                            <span class="image"><img src="images/studentpage/man1.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a>  film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>

                        </li>
                        <li class="list-group-item">
                            <span class="image"><img src="images/studentpage/man.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a> film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>

                        </li>
                        <li class="list-group-item">
                            <span class="image"><img src="images/studentpage/girl.png" class="img-circle" alt="Profile Image"></span>
                            <span class="user_discuss">
                        <a href="">John Smith</a>film festivals used to be do-or-die moments for movie makers.</span>
                            <span class="time">3 mins ago</span>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>