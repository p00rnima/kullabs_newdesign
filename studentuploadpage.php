<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Upload page</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-9">
            <!-- D&D Zone-->
            <div id="drag-and-drop-zone" class="uploader">
                <div>Drag &amp; Drop Notes Here</div>
                <div class="or">-or-</div>
                <div class="browser">
                    <label>
                        <span>Click to open the file Browser</span>
                        <input type="file" name="files[]" multiple="multiple" title='Click to add Files'>
                    </label>

                </div>
                <input type="submit" name="Upload" class="user_upload">
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>