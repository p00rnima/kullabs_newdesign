<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
        <div>
            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>
        </div>
    </div>
    <div class="row">
        <div class="lesson-page">
            <div class="col-md-9">
                <h4>Search school from A - Z</h4>
                <div class="glossary">
                    <a href="#">A</a>
                    <a href="#">B</a>
                    <a href="#">C</a>
                    <a href="#">D</a>
                    <a href="#">E</a>
                    <a href="#">F</a>
                    <a href="#">G</a>
                    <a href="#">H</a>
                    <a href="#">I</a>
                    <a href="#">J</a>
                    <a href="#">K</a>
                    <a href="#">L</a>
                    <a href="#">M</a>
                    <a href="#">N</a>
                    <a href="#">O</a>
                    <a href="#">P</a>
                    <a href="#">Q</a>
                    <a href="#">R</a>
                    <a href="#">S</a>
                    <a href="#">T</a>
                    <a href="#">U</a>
                    <a href="#">V</a>
                    <a href="#">W</a>
                    <a href="#">X</a>
                    <a href="#">Y</a>
                    <a href="#">Z</a>
                </div>
                <table id="example" class="searchTable table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr class="header">
                        <th>Logo</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#" class="active"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Islington College Pvt.</a></td>
                        <td>KamalPokhari, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo1.png" class="logo-rectangle"></a></td>
                        <td class="list-group-item"><a href="#">Softwarica College of IT & E-commerce</a></td>
                        <td>Dillibazar, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo2.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">ISMT College </a></td>
                        <td>Tinkune, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo4.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Kathmandu Medical College</a></td>
                        <td>Bagbazar, Kathmandu</td>
                        <td>01-5550223</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">The British College </a></td>
                        <td>Thapathali, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Khowpa Engineering College</a></td>
                        <td>Suryabinayak, Bhaktapur</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Daffodil English Boarding School</a></td>
                        <td>Kapan, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Suryodaya School</a></td>
                        <td>Ghattekulo, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Trichandra Campus</a></td>
                        <td>Ghantaghar, Kathmandu</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo1.png" class="logo-rectangle"></a></td>
                        <td class="list-group-item"><a href="#">Himalayan Engineering Campus</a></td>
                        <td>Balkumari, Laltipur</td>
                        <td>01-5554323</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo1.png" class="logo-rectangle"></a></td>
                        <td class="list-group-item"><a href="#">Trinity Int'l College</a></td>
                        <td>Dillibazar Height, Kathmandu</td>
                        <td>01-5514320</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Kathmandu University</a></td>
                        <td>Dulikhel, Kavre</td>
                        <td>01-6661377</td>
                    </tr>
                    <tr>
                        <td><a href="#"><img src="images/college-logos/logo3.png" class="logo-circle"></a></td>
                        <td class="list-group-item"><a href="#">Nepal Commerce Campus</a></td>
                        <td>Minbhawan, Kathmandu</td>
                        <td>01-4491377</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- END OF col-md-9 -->
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/main.js"></script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!-- START OF search result -->
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


</body>
</html>