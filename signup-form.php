<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KulLabs Pvt.</title>
    <!--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!--<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row mg-btm">
                <div class="col-xs-6 col-md-8">
                    <div class="login-head">
                        <!--<img src="ronaldo.jpg" alt="profile_pic">-->
                        <!--<img src="user.png" alt="profile_pic">-->
                        <img src="images/user.png" alt="profile_pic">
                    </div>
                </div>
            </div>
            <div class="form-signin mg-btm">
                <h2 class="heading-desc">Signup to KulLabs</h2>

                <div class="main">
                    <form id="loginForm" action="#" method="POST">
                        <div class="login-form">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group has-primary has-feedback">
                                        <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" placeholder="First Name " id="">

                                    </div>
                                    <div class="form-group has-primary has-feedback">
                                        <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" placeholder="Last Name" id="">

                                    </div>
                                    <div class="form-group has-primary has-feedback">
                                        <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" placeholder="Username " id="UserName">

                                    </div>
                                    <div class="form-group has-primary has-feedback">
                                        <i class="fa fa-envelope"></i>
                                        <input type="text" class="form-control" placeholder="Enter Email " id="">

                                    </div>
                                    <div class="form-group has-primary has-feedback">
                                        <div class="form-group log-status">
                                            <i class="fa fa-lock"></i>
                                            <input type="password" class="form-control" placeholder="Password" id="Passwod">

                                        </div>
                                    </div>

                                    <div class="form-group has-primary has-feedback">
                                        <div class="form-group log-status">
                                            <i class="fa fa-lock"></i>
                                            <input type="password" class="form-control" placeholder="Re-Password" id="Passwod">

                                        </div>

                                    </div>

                                    <div class="form-group has-primary has-feedback">
                                        <div class="form-group log-status">
                                            <!--<label for="Country">Country</label>-->
                                            <select id="country" name="country" autocomplete="country" class="form-control">
                                                <option value="" selected="selected">(please select a user type)</option>
                                                <option value="AF">Teacher</option>
                                                <option value="AL">Parent</option>
                                                <option value="DZ">Student</option>
                                            </select>


                                        </div>
                                    </div>
                                    <center style="margin-bottom:12px;"> Already have an account? <a href="login.php"> Sign Up Here</a></center>
                                    <div class="form-group has-primary has-feedback">
                                        <span class="alert">Invalid Credentials</span>

                                        <button type="button" class="log-btn" >Sign Up</button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</body>
</html>