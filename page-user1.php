<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>

      <div class="content-wrapper">
        <div class="row user">
          <div class="col-md-12">
            <div class="profile">
              <div class="info"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg" class="user-img">
                <h4>Shuva Sharma</h4>
                <p>Student (New Summit College)</p>
              </div>
              <div class="cover-image"></div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card p-0">
              <ul class="nav nav-tabs nav-stacked user-tabs">
                <li class="active"><a href="#user-timeline" data-toggle="tab">Timeline</a></li>
                <li><a href="#user-info" data-toggle="tab">Info</a></li>
                <li><a href="#user-settings" data-toggle="tab">Settings</a></li>
              </ul>
            </div>
<!--              --><?php //include_once ('right-sidebar.php') ?>
          </div>
          <div class="col-md-9">
            <div class="tab-content">
              <div id="user-timeline" class="tab-pane active">
                <div class="timeline">
                  <div class="post">
                    <div class="post-media"><a href="#"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"></a>
                      <div class="content">
                        <h5><a href="#">John Doe</a></h5>
                        <p class="text-muted"><small>2 January at 9:30</small></p>
                      </div>
                    </div>
                    <div class="post-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,	quis tion ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <ul class="post-utility">
                      <li class="likes"><a href="#"><i class="fa fa-fw fa-lg fa-thumbs-o-up"></i>Like</a></li>
                      <li class="shares"><a href="#"><i class="fa fa-fw fa-lg fa-share"></i>Share</a></li>
                      <li class="comments"><i class="fa fa-fw fa-lg fa-comment-o"></i> 5 Comments</li>
                    </ul>
                  </div>
                  <div class="post">
                    <div class="post-media"><a href="#"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"></a>
                      <div class="content">
                        <h5><a href="#">John Doe</a></h5>
                        <p class="text-muted"><small>2 January at 9:30</small></p>
                      </div>
                    </div>
                    <div class="post-content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,	quis tion ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <ul class="post-utility">
                      <li class="likes"><a href="#"><i class="fa fa-fw fa-lg fa-thumbs-o-up"></i>Like</a></li>
                      <li class="shares"><a href="#"><i class="fa fa-fw fa-lg fa-share"></i>Share</a></li>
                      <li class="comments"><i class="fa fa-fw fa-lg fa-comment-o"></i> 5 Comments</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="user-info" class="tab-pane fade">
                    <div class="card user-settings">
                        <h4 class="line-head">Info</h4>
                        <!-- start FORM -->
                        <br style="clear:both">
                        <a class="pull-left" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="showWorkForm">
                            <i class="fa fa-plus add-title" aria-hidden="true"></i> Add Work </a>
                        <br style="clear:both">
                        <!-- start WORK collapse form -->
                        <div class="collapse" id="showWorkForm">
                            <div class="col-md-12">
                                <!--<div class="form-area"  id="current-pane">-->
                                <div class="form-area">
                                    <form role="form" style="margin-bottom: 10%" id="collapseOnClose" class="collapse in">
                                        <!--<a class="pull-right" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                        <br style="clear:both">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Company Name" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Position" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="City" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" type="textarea" id="message" placeholder="Description" maxlength="140" rows="3"></textarea>
                                            <!--<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>-->
                                        </div>
                                        <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                        <button type="button" id="submit" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                        <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#showWorkForm" aria-expanded="false" aria-controls="showWorkForm"> Close </a>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end WORK collapse form -->
                        <hr class="" style="clear:both">

                        <!-- start COLLEGE -->
                        <a class="pull-left" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="showCollegeForm">
                            <i class="fa fa-plus add-title" aria-hidden="true"></i>
                            Add College
                        </a>
                        <br style="clear:both">

                        <div class="collapse" id="showCollegeForm">
                            <!-- start collapse form -->
                            <div class="col-md-12">
                                <!--<div class="form-area"  id="current-pane">-->
                                <div class="form-area">
                                    <form role="form" id="collapseOnClose2" class="form-margin collapse in">
                                        <!--<a class="pull-right" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                        <br style="clear:both">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Company Name" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Position" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="mobile" placeholder="City" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" type="textarea" placeholder="Description" maxlength="140" rows="3"></textarea>
                                            <!--<span class="help-block"><p class="help-block ">You have reached the limit</p></span>-->
                                        </div>
                                        <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                        <button type="button" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                        <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#showCollegeForm" aria-expanded="false" aria-controls="showCollegeForm"> Close </a>
                                    </form>
                                </div>
                            </div> <!-- end collapse form -->
                        </div>
                        <!-- end COLLEGE -->

                        <hr class="hr-collapse-form" style="clear:both">
                        <h3 class="profile-title" style=""> <i class="title-icon fa fa-briefcase" aria-hidden="true"></i> KulLabs Pvt. Ltd.</h3>
                        <hr/>
                        <h3 class="profile-title"> <i class="title-icon fa fa-university" aria-hidden="true"></i> Islington College</h3>
                        <hr/>
                        <h3> <i class="title-icon fa fa-briefcase" aria-hidden="true"></i> <span class="profile-title"> KulLabs Pvt. Ltd.</span> <a class="pull-right" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="showWorkForm"> Edit Your Work</a></h3>

                        <!-- start edit WORK collapse form -->
                        <div class="collapse" id="editWorkForm">
                            <div style="padding:19px;">
                                <!--<div class="form-area"  id="current-pane">-->
                                <div class="form-area">
                                    <form role="form" id="collapseOnClose3" class="form-margin collapse in">
                                        <!--<a class="pull-right" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="editWorkForm" aria-label="close"><h2>&times;</h2></a>-->
                                        <br style="clear:both">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Company Name" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Position" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="mobile" placeholder="City" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" type="textarea" placeholder="Description" maxlength="140" rows="3"></textarea>
                                            <!--<span class="help-block"><p class="help-block ">You have reached the limit</p></span>-->
                                        </div>
                                        <!--<button href="#current-pane" type="button" class="btn btn-default" data-dismiss="alert" aria-label="close" id="hide"  >Close</button>-->
                                        <button type="button" name="submit" class="btn btn-primary pull-right" style="margin-left: 1%">Submit</button>
                                        <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#editWorkForm" aria-expanded="false" aria-controls="editWorkForm"> Close </a>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
              <div id="user-settings" class="tab-pane fade">
                <div class="card user-settings">
                  <h4 class="line-head">Settings</h4>
                  <form>
                    <div class="row mb-20">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-8 mb-20">
                        <label>Email</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-md-8 mb-20">
                        <label>Mobile No</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-md-8 mb-20">
                        <label>Office Phone</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-md-8 mb-20">
                        <label>Home Phone</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="row mb-10">
                      <div class="col-md-12">
                        <button type="button" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Javascripts-->
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/essential-plugins.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/pace.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>