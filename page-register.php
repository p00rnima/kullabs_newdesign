<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
    <title>Kullabs.com</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    -->
</head>
<body>
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="login-content">
    <div class="logo">
        <h1>Kullabs.com</h1>
    </div>
    <div class="login-box" style="min-height:744px;">
        <form action="index.html" class="login-form">
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Register</h3>
            <div class="form-group">
                <label class="control-label">Name</label>
                <input type="text" placeholder="Name" autofocus class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">USERNAME</label>
                <input type="text" placeholder="Username" autofocus class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">EMAIL</label>
                <input type="text" placeholder="Email" autofocus class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">PASSWORD</label>
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">RE-PASSWORD</label>
                <input type="password" placeholder="Re-Password" class="form-control">
            </div>
            <div class="form-group">
                <select name="" class="form-control" style="padding:0;">
                <option value="volvo">(Please select a user type)</option>
                <option value="volvo">Student</option>
                <option value="saab">Teacher</option>
                <option value="mercedes">Parent</option>
            </select>
            </div>
            <div class="form-group">
                <div class="utility">
                    <div class="animated-checkbox">
                        <label class="semibold-text">
                            <span class="label-text">Already have an account? </span>
                        </label>
                    </div>
                    <p class="semibold-text mb-0"><a id="toFlip" href="#">Sign In Here</a></p>
                </div>
            </div>
            <div class="form-group btn-container">
                <button class="btn btn-primary btn-block">Sign Up <i class="fa fa-sign-in fa-lg"></i></button>
            </div>
            <div class="login-option">
                <h5>OR</h5>
                <p>
                    Sign in with a Social Account
                </p>
                <div class="social_account">
                    <a href="#" class="btn btn-social btn-google">
                        <i class="fa fa-google"></i>
                    </a>
                    <a href="#" class="btn btn-social btn-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-social btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="btn btn-social btn-linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </form>


    </div>
</section>
</body>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</html>