      <!-- header  -->
        <?php include_once('header.php') ?>
      <!-- Side-Nav-->
        <?php include_once('sidenav.php') ?>  


      <!-- content -->

      
      <div class="content-wrapper">
        <div class="page-title">
          <!--  <div>
            <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
            <p> admin template</p>
            </div> -->
          <div>
              <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About us</a></li>
                  <li class="active">Exposure</li>
              </ul>
          </div>
          <div>
            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>
          </div>
        </div>

        <div class="exposure-detail">
          <div class="col-md-9">
            <div class="exposure-list">
              <div class="customNavigation">
                <a class="btn prev"><i class="fa fa-angle-left"></i></a>
                <a class="btn next"><i class="fa fa-angle-right"></i></a>
              </div>
              <div id="owl-demo" class="" ss="owl-carousel">
                <div class="detail">
                  <div class = "caption category_title active" data-id="category01">
                    <p>upload notes</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category02">
                    <p>Videos</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category03">
                    <p>Music</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category04">
                    <p>Blog</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category05">
                    <p>Free E-Books</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category06">
                    <p>Forum</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category07">
                    <p>Advertisment</p>
                  </div>
                </div>
                <div class="detail">
                  <div class ="caption category_title" data-id="category08">
                    <p>Free E-Books</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="exposure-subdetail">
                  <div id="category01">
                      <div class="well school-info">
                          <div class="media media-detail">
                              <div class="media-left media-top">
                                  <a href="#">
                                      <img class="media-object" src="images/female.jpg" alt="...">
                                  </a>
                              </div>
                              <div class="media-body media01">
                                  <h4 class="media-heading"><strong>Kullabs School</strong> uploaded a note on Basic Definitions,  Time Domain Expressions For FM and PM</h4>
                                  <p class="small text-muted"><span class="fa fa-clock-o"></span>25 December 2015</p>
                                  <p>Lसुन्दा अचम्म लाग्ने कुरा हो, तर कल्पना गर्नुहोस्, ग्लोबल वार्मिंगले बढ्दो तापमान र जलवायु परिवर्तनले नेपाल मरुभूमिमा परिणत हुन
                                      सक्ने अवस्था सिर्जना भइराखेको छ। तपाईंले नेपालमा गर्मी महिनामा धेरै गर्मि महसुस गरेको हुनुहुन्छ नै तर ज
                                      ाडो महिनामा आजकल दिनभर न्यानो पनि हुने गर्छ? मनसुन पहिलेजस्त्तै समयमा आइपुग्ने गर्दैन र पहिले...
                                  </p>
                                  <img src="images/40.jpg" class="img-responsive" style="width:683px;height:257px">
                                  <li class="views_boost">
                                      <button class="boost-btn pull-right" data-toggle="modal" data-target="#boost_popup"><i class="fa fa-tint" aria-hidden="true"></i> Boost Post </button>
                                      <a href="#">
                                          <p> 7971 Views</p>
                                      </a>
                                      <!-- start BOOST POST popup -->
                                      <section class="popup-form">
                                          <div class="modal fade" id="boost_popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                                               aria-hidden="true">
                                              <div class="modal-dialog modal-lg">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                              &times;</button>
                                                          <h4 class="modal-title" id="myModalLabel">
                                                              Boost Your Post
                                                          </h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                              <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                                                                  <div class="scrollBox">
                                                                      <!-- Tab panes -->
                                                                      <form role="form" class="form-horizontal scroll_form">
                                                                          <div class="form-group">
                                                                              <label for="exampleInputEmail1"  class="pull-left">Your Details</label>
                                                                              <div class="input col-md-8 pull-left" style="margin: 30px 0px 0px -95px;">
                                                                                  <input type="email" placeholder="Enter your name" id="exampleInputEmail1" class="form-control">
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label> Audience</label>
                                                                              <div class="radio">
                                                                                  <label><input type="radio" name="optradio">People you choose through targeting</label>
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label for="sel1" class="pull-left">Budget:</label>
                                                                              <div class="col-sm-4 col-md-4 pull-left" style="margin: 30px 0px 0px -70px;">
                                                                                  <select class="form-control" id="sel1">
                                                                                      <option>1</option>
                                                                                      <option>2</option>
                                                                                      <option>3</option>
                                                                                      <option>4</option>
                                                                                  </select>
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label class="pull-left"> Date</label>
                                                                              <div class="col-md-5 pull-left" style="margin: 30px 0px 0px -45px;">
                                                                                  <input class="form-control" type="date" name="date">
                                                                              </div>
                                                                          </div>
                                                                          <p style="margin: 30px 0px 0px -10px;"><label> About Us</label>
                                                                              People who use your app or site have a particular goal. Often the one thing that is standing between the user and his goal is a form. Because forms still remain the one of the most important type of interaction for users on the web and in the apps. In fact, forms are often considered to be the final step of the journey to the completion of goals.
                                                                          </p>
                                                                          </p>
                                                                          <div class="form-group">
                                                                              <div class="col-sm-10">
                                                                                  <button type="submit" class="btn btn-primary btn-sm pull-right">
                                                                                      Submit</button>
                                                                              </div>
                                                                          </div>
                                                                      </form>
                                                                  </div>
                                                                  <!--<div id="OR" class="hidden-xs">
                                                                    OR
                                                                    </div>-->
                                                              </div>
                                                              <div class="col-md-4">
                                                                  <label>Preview</label>
                                                                  <img src="images/8.jpg" class="img-responsive">
                                                              </div>
                                                          </div>
                                                          <div class="modal-footer col-md-12 boost-footer">
                                                              <p class="pull-left"> By clicking Boost, you agree to <a href="#" title="KulLabs's Terms & Conditions"> KulLabs's Terms & Conditions </a> | <a href="#" title="Help"> Help </a></p>
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary">Boost Post</button>
                                                          </div>
                                                      </div>
                                                      <!-- end MODAL-BODY -->
                                                  </div>
                                              </div>
                                          </div>
                                      </section>
                                      <!-- end BOOST POST popup -->
                                  </li>
                                  <li class="status-message">
                                      <div class="follow">
                                          <a href="#"><span class="fa fa-thumbs-up fa-fw"></span>Encourage</a>
                                          <a href="#"><span class="fa fa-comment fa-fw"></span>comment</a>
                                          <!--  <div class="dropdown share-option">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Share
                                                <span class="fa fa-share"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Reshare</a></li>
                                                <li><a href="#">Facebook</a></li>
                                                <li><a href="#">Twitter</a></li>
                                            </ul>
                                            </div> -->
                                          <a href="#"  data-toggle="modal" data-target="#reportPopup" title="Report"><span class="fa fa-flag fa-fw"></span>Report Problem</a>
                                          <!-- start Popup Report Modal -->
                                          <div class="modal fade" id="reportPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <h4 class="modal-title" id="myModalLabel">Report Your Problems</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                              <form role="form" id="contact-form" class="contact-form">
                                                                  <div class="col-md-6">
                                                                      <div class="form-group">
                                                                          <input type="text" class="form-control" name="Name" id="Name" placeholder="Name">
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-6">
                                                                      <div class="form-group">
                                                                          <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-12">
                                                                      <div class="form-group">
                                                                          <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message"></textarea>
                                                                      </div>
                                                                  </div>
                                                                  <!--<div class="col-md-6">
                                                                    <button type="submit" class="btn main-btn pull-right">Send a message</button>
                                                                    </div>-->
                                                              </form>
                                                          </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Send Report</button>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!-- end Popup Report Modal -->
                                          <div id="stars-default" class="star-css" style="float:right;"><input type=hidden name="rating"><span class="label label-default star-rating">5</span></div>
                                          </a>
                                      </div>
                                  </li>
                                  <div class="media">
                                      <div class="media-left">
                                          <a href="#">
                                              <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                          </a>
                                      </div>
                                      <div class="media-body">
                                          <div class="form-group">
                                              <textarea class="form-control input-sm" rows="2" id="comment" style="height:35px;"></textarea>
                                              <button type="button" class="btn btn-sm btn-primary submit-button" style="margin-top:10px;"> Submit Comment </button>
                                          </div>
                                      </div>
                                  </div>

                                  <div>
                                      <div class="media">
                                          <div class="media-left">
                                              <a href="#">
                                                  <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                              </a>
                                          </div>
                                          <div class="media-body comment-list">
                                              <p class="media-heading"> <span class="comment_user_name"> Madan Pokharel : </span>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                                  Cras purus odio <span class=" pull-right fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></span>  <span class=" pull-right fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="delete"></span>
                                              </p>
                                          </div>
                                      </div>
                                      <div class="media">
                                          <div class="media-left">
                                              <a href="#">
                                                  <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                              </a>
                                          </div>
                                          <div class="media-body comment-list">
                                              <p class="media-heading"> <span class="comment_user_name"> Madan Pokharel : </span>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                                  Cras purus odio
                                              </p>
                                          </div>
                                      </div>
                                      <p>  <a href="#"> view more comments </a> </p>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
                  <div id="category02" class="custom-hidden">
                  </div>
                  <div id="category03" class="custom-hidden">
                  </div>
                  <div id="category04" class="custom-hidden">
                  </div>
                  <div id="category05" class="custom-hidden">
                  </div>
                  <div id="category06" class="custom-hidden">
                  </div>
                  <div id="category07" class="custom-hidden">
                  </div>
                  <div id="category08" class="custom-hidden">
                  </div>
              </div>
              <div class="exposure-subdetail">
                  <div id="category01">
                      <div class="well school-info">
                          <div class="media media-detail">
                              <div class="media-left media-top">
                                  <a href="#">
                                      <img class="media-object" src="images/female.jpg" alt="...">
                                  </a>
                              </div>
                              <div class="media-body media01">
                                  <h4 class="media-heading"><strong>Kullabs School</strong> uploaded a note on Basic Definitions,  Time Domain Expressions For FM and PM</h4>
                                  <p class="small text-muted"><span class="fa fa-clock-o"></span>25 December 2015</p>
                                  <p>Lसुन्दा अचम्म लाग्ने कुरा हो, तर कल्पना गर्नुहोस्, ग्लोबल वार्मिंगले बढ्दो तापमान र जलवायु परिवर्तनले नेपाल मरुभूमिमा परिणत हुन
                                      सक्ने अवस्था सिर्जना भइराखेको छ। तपाईंले नेपालमा गर्मी महिनामा धेरै गर्मि महसुस गरेको हुनुहुन्छ नै तर ज
                                      ाडो महिनामा आजकल दिनभर न्यानो पनि हुने गर्छ? मनसुन पहिलेजस्त्तै समयमा आइपुग्ने गर्दैन र पहिले...
                                  </p>
                                  <img src="images/40.jpg" class="img-responsive" style="width:683px;height:257px">
                                  <li class="views_boost">
                                      <button class="boost-btn pull-right" data-toggle="modal" data-target="#boost_popup"><i class="fa fa-tint" aria-hidden="true"></i> Boost Post </button>
                                      <a href="#">
                                          <p> 7971 Views</p>
                                      </a>
                                      <!-- start BOOST POST popup -->
                                      <section class="popup-form">
                                          <div class="modal fade" id="boost_popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                                               aria-hidden="true">
                                              <div class="modal-dialog modal-lg">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                              &times;</button>
                                                          <h4 class="modal-title" id="myModalLabel">
                                                              Boost Your Post
                                                          </h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                              <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                                                                  <div class="scrollBox">
                                                                      <!-- Tab panes -->
                                                                      <form role="form" class="form-horizontal scroll_form">
                                                                          <div class="form-group">
                                                                              <label for="exampleInputEmail1"  class="pull-left">Your Details</label>
                                                                              <div class="input col-md-8 pull-left" style="margin: 30px 0px 0px -95px;">
                                                                                  <input type="email" placeholder="Enter your name" id="exampleInputEmail1" class="form-control">
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label> Audience</label>
                                                                              <div class="radio">
                                                                                  <label><input type="radio" name="optradio">People you choose through targeting</label>
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label for="sel1" class="pull-left">Budget:</label>
                                                                              <div class="col-sm-4 col-md-4 pull-left" style="margin: 30px 0px 0px -70px;">
                                                                                  <select class="form-control" id="sel1">
                                                                                      <option>1</option>
                                                                                      <option>2</option>
                                                                                      <option>3</option>
                                                                                      <option>4</option>
                                                                                  </select>
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                              <label class="pull-left"> Date</label>
                                                                              <div class="col-md-5 pull-left" style="margin: 30px 0px 0px -45px;">
                                                                                  <input class="form-control" type="date" name="date">
                                                                              </div>
                                                                          </div>
                                                                          <p style="margin: 30px 0px 0px -10px;"><label> About Us</label>
                                                                              People who use your app or site have a particular goal. Often the one thing that is standing between the user and his goal is a form. Because forms still remain the one of the most important type of interaction for users on the web and in the apps. In fact, forms are often considered to be the final step of the journey to the completion of goals.
                                                                          </p>
                                                                          </p>
                                                                          <div class="form-group">
                                                                              <div class="col-sm-10">
                                                                                  <button type="submit" class="btn btn-primary btn-sm pull-right">
                                                                                      Submit</button>
                                                                              </div>
                                                                          </div>
                                                                      </form>
                                                                  </div>
                                                                  <!--<div id="OR" class="hidden-xs">
                                                                    OR
                                                                    </div>-->
                                                              </div>
                                                              <div class="col-md-4">
                                                                  <label>Preview</label>
                                                                  <img src="images/8.jpg" class="img-responsive">
                                                              </div>
                                                          </div>
                                                          <div class="modal-footer col-md-12 boost-footer">
                                                              <p class="pull-left"> By clicking Boost, you agree to <a href="#" title="KulLabs's Terms & Conditions"> KulLabs's Terms & Conditions </a> | <a href="#" title="Help"> Help </a></p>
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary">Boost Post</button>
                                                          </div>
                                                      </div>
                                                      <!-- end MODAL-BODY -->
                                                  </div>
                                              </div>
                                          </div>
                                      </section>
                                      <!-- end BOOST POST popup -->
                                  </li>
                                  <li class="status-message">
                                      <div class="follow">
                                          <a href="#"><span class="fa fa-thumbs-up fa-fw"></span>Encourage</a>
                                          <a href="#"><span class="fa fa-comment fa-fw"></span>comment</a>
                                          <!--  <div class="dropdown share-option">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Share
                                                <span class="fa fa-share"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Reshare</a></li>
                                                <li><a href="#">Facebook</a></li>
                                                <li><a href="#">Twitter</a></li>
                                            </ul>
                                            </div> -->
                                          <a href="#"  data-toggle="modal" data-target="#reportPopup" title="Report"><span class="fa fa-flag fa-fw"></span>Report Problem</a>
                                          <!-- start Popup Report Modal -->
                                          <div class="modal fade" id="reportPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <h4 class="modal-title" id="myModalLabel">Report Your Problems</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                              <form role="form" id="contact-form" class="contact-form">
                                                                  <div class="col-md-6">
                                                                      <div class="form-group">
                                                                          <input type="text" class="form-control" name="Name" id="Name" placeholder="Name">
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-6">
                                                                      <div class="form-group">
                                                                          <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-12">
                                                                      <div class="form-group">
                                                                          <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message"></textarea>
                                                                      </div>
                                                                  </div>
                                                                  <!--<div class="col-md-6">
                                                                    <button type="submit" class="btn main-btn pull-right">Send a message</button>
                                                                    </div>-->
                                                              </form>
                                                          </div>
                                                      </div>
                                                      <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Send Report</button>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!-- end Popup Report Modal -->
                                          <div id="stars-default" class="star-css" style="float:right;"><input type=hidden name="rating"><span class="label label-default star-rating">5</span></div>
                                          </a>
                                      </div>
                                  </li>
                                  <div class="media">
                                      <div class="media-left">
                                          <a href="#">
                                              <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                          </a>
                                      </div>
                                      <div class="media-body">
                                          <div class="form-group">
                                              <textarea class="form-control input-sm" rows="2" id="comment" style="height:35px;"></textarea>
                                              <button type="button" class="btn btn-sm btn-primary submit-button" style="margin-top:10px;"> Submit Comment </button>
                                          </div>
                                      </div>
                                  </div>

                                  <div>
                                      <div class="media">
                                          <div class="media-left">
                                              <a href="#">
                                                  <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                              </a>
                                          </div>
                                          <div class="media-body comment-list">
                                              <p class="media-heading"> <span class="comment_user_name"> Madan Pokharel : </span>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                                  Cras purus odio
                                              </p>
                                          </div>
                                      </div>
                                      <div class="media">
                                          <div class="media-left">
                                              <a href="#">
                                                  <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                              </a>
                                          </div>
                                          <div class="media-body comment-list">
                                              <p class="media-heading"> <span class="comment_user_name"> Madan Pokharel : </span>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                                  Cras purus odio
                                              </p>
                                          </div>
                                      </div>
                                      <p>  <a href="#"> view more comments </a> </p>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
                  <div id="category02" class="custom-hidden">
                  </div>
                  <div id="category03" class="custom-hidden">
                  </div>
                  <div id="category04" class="custom-hidden">
                  </div>
                  <div id="category05" class="custom-hidden">
                  </div>
                  <div id="category06" class="custom-hidden">
                  </div>
                  <div id="category07" class="custom-hidden">
                  </div>
                  <div id="category08" class="custom-hidden">
                  </div>
              </div>
          </div>
            <!-- Right-Sidebar-->
            <?php include_once('right-sidebar.php') ?>
        </div>
      </div>
    </div>
    <!-- Javascripts-->
      <script src="js/jquery-2.1.4.min.js"></script>
     <script src="js/bootstrap.min.js"></script>
    <script src="js/essential-plugins.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/exposureslider.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">        
      $('#theCarousel').carousel({
      interval: false
      })
      
      $('.multi-item-carousel .item').each(function(){
      var next = $(this).next();
      if (!next.length) {
      next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      
      if (next.next().length>0) {
      next.next().children(':first-child').clone().appendTo($(this));
      }
      else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
      $(".owl-item").css("width","110px !important");
      });
    </script>
    <script>
      $(function(){
          $(".category_title").on('click', function(){
              var dataId = $(this).attr("data-id");
              console.log(dataId);
              hideAll();
              $(this).addClass("category-active");
              $("#"+dataId).show();
          });
          function hideAll(){
              $(".category_title p").removeClass("category-active");
              $("#category01").hide();
              $("#category02").hide();
              $("#category03").hide();
              $("#category04").hide();
              $("#category05").hide();
          }
      });
    </script>
    <!-- ================== start rating ====================== -->
    <script type="text/javascript">
      //the $(document).ready() function is down at the bottom
      //the $(document).ready() function is down at the bottom
      
      (function ( $ ) {
      
      $.fn.rating = function( method, options ) {
      method = method || 'create';
          // This is the easiest way to have default options.
          var settings = $.extend({
              // These are the defaults.
        limit: 5,
        value: 0,
        glyph: "glyphicon-star",
              coloroff: "gray",
        coloron: "gold",
        size: "1.5em",
        cursor: "default",
        onClick: function () {},
              endofarray: "idontmatter"
          }, options );
      var style = "";
      style = style + "font-size:" + settings.size + "; ";
      style = style + "color:" + settings.coloroff + "; ";
      style = style + "cursor:" + settings.cursor + "; ";
      
      
      
      if (method == 'create')
      {
        //this.html('');  //junk whatever was there
        
        //initialize the data-rating property
        this.each(function(){
          attr = $(this).attr('data-rating');
          if (attr === undefined || attr === false) { $(this).attr('data-rating',settings.value); }
        })
        
        //bolt in the glyphs
        for (var i = 0; i < settings.limit; i++)
        {
          this.append('<span data-value="' + (i+1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
        }
        
        //paint
        this.each(function() { paint($(this)); });
      
      }
      if (method == 'set')
      {
        this.attr('data-rating',options);
        this.each(function() { paint($(this)); });
      }
      if (method == 'get')
      {
        return this.attr('data-rating');
      }
      //register the click events
      this.find("span.ratingicon").click(function() {
        rating = $(this).attr('data-value')
        $(this).parent().attr('data-rating',rating);
        paint($(this).parent());
        settings.onClick.call( $(this).parent() );
      })
      function paint(div)
      {
        rating = parseInt(div.attr('data-rating'));
        div.find("input").val(rating);  //if there is an input in the div lets set it's value
        div.find("span.ratingicon").each(function(){  //now paint the stars
          
          var rating = parseInt($(this).parent().attr('data-rating'));
          var value = parseInt($(this).attr('data-value'));
          if (value > rating) { $(this).css('color',settings.coloroff); }
          else { $(this).css('color',settings.coloron); }
        })
      }
      
      };
      
      }( jQuery ));
      
      $(document).ready(function(){
      
      $("#stars-default").rating();
      $("#stars-green").rating('create',{coloron:'green',onClick:function(){ alert('rating is ' + this.attr('data-rating')); }});
      $("#stars-herats").rating('create',{coloron:'red',limit:10,glyph:'glyphicon-heart'}); 
      });
      
    </script>
      <script>
          $(document).ready(function() {
              $('[data-toggle="tooltip"]').tooltip()
          })
      </script>
    <!-- ================== start rating end====================== -->
  </body>
</html>