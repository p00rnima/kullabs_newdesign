<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">School Detail</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.html" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">

            <div class="school-detail">
                <div class="col-md-9">
                    <div class="ads-space">
                        <img src="images/ads.PNG" class="img-responsive">
                    </div>

                    <div class="well school-info">
                        <div class="media">
                            <div class="media-left media-middle">
                                <a href="#">
                                    <img class="media-object" src="images/kullabs_admin.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><strong>Kullabs Smart School</strong></h4>
                                <p><span class="fa  fa-map-marker"></span> Putalisadk , Mobile Bazaar</p>
                                <p><span class="fa fa-envelope"></span> kullabs@gmail.com</p>
                                <p><span class="fa fa-globe"></span> www.kullabs.com</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                <a href=""><p> view more &raquo </p></a>
                            </div>
                        </div>
                    </div>
                    <div class="well school-info">
<!--                        <form class="form-horizontal post-form" role="form">-->
<!--                            <h4>What's New</h4>-->
<!--                            <div class="form-group" style="padding:14px;">-->
<!--                                <textarea class="form-control" placeholder="Update your status"></textarea>-->
<!--                            </div>-->
<!--                            <button class="btn btn-primary" type="button">Post</button>-->
<!--                        </form>-->
                        <div class="panel rounded shadow">
                            <form action="...">
                                <textarea class="form-control input-lg no-border" rows="2" placeholder="What are you doing?..."></textarea>
                            </form>
                            <div class="panel-footer">
                                <button class="btn btn-success pull-right mt-5">POST</button>
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-user"></i></a></li>
                                    <li><a href="#"><i class="fa fa-map-marker"></i></a></li>
                                    <li><a href="#"><i class="fa fa-camera"></i></a></li>
                                    <li><a href="#"><i class="fa fa-smile-o"></i></a></li>
                                </ul><!-- /.nav nav-pills -->
                            </div><!-- /.panel-footer -->
                        </div><!-- /.panel -->
                    </div>
                    <div class="well school-info">
                        <div class="media media-detail">
                            <div class="media-left media-top">
                                <a href="#">
                                    <img class="media-object" src="images/female.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body media01">
                                <h4 class="media-heading"><strong>Kullabs School</strong> uploaded a note on Basic Definitions,  Time Domain Expressions For FM and PM</h4>
                                <p class="small text-muted"><span class="fa fa-clock-o"></span>25 December 2015</p>
                            </div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s</p>
                            <li class="status-message">
                                <div class="follow">
                                    <a href="#"><span class="fa fa-thumbs-up fa-fw"></span>Encourage</a>
                                    <a href="#"><span class="fa fa-comment fa-fw"></span>comment</a>
                                    <div class="dropdown share-option">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Share
                                            <span class="fa fa-share"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Reshare</a></li>
                                            <li><a href="#">Facebook</a></li>
                                            <li><a href="#">Twitter</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <form>
                                        <input type="text" class="form-control" placeholder="Add a comment..">
                                    </form>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body comment-list">
                                        <p class="media-heading">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                            Cras purus odio</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="well school-info">
                        <div class="media media-detail">
                            <div class="media-left media-top">
                                <a href="#">
                                    <img class="media-object" src="images/female.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body media01">
                                <h4 class="media-heading"><strong>Kullabs School</strong> uploaded a note on Basic Definitions,  Time Domain Expressions For FM and PM</h4>
                                <p class="small text-muted"><span class="fa fa-clock-o"></span>25 December 2015</p>
                            </div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s</p>
                            <img src="images/Orienation.jpg" class="img-responsive" style="width:683px;height:257px">
                            <li class="status-message">
                                <div class="follow">
                                    <a href="#"><span class="fa fa-thumbs-up fa-fw"></span>Encourage</a>
                                    <a href="#"><span class="fa fa-comment fa-fw"></span>comment</a>
                                    <div class="dropdown share-option">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Share
                                            <span class="fa fa-share"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Reshare</a></li>
                                            <li><a href="#">Facebook</a></li>
                                            <li><a href="#">Twitter</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <form>
                                        <input type="text" class="form-control" placeholder="Add a comment..">
                                    </form>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="images/female.jpg" style="width:28px; height:28px" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body comment-list">
                                        <p class="media-heading">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                            Cras purus odio</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="well school-info">
                        <div class="media media-detail">
                            <div class="media-left media-top">
                                <a href="#">
                                    <img class="media-object" src="images/female.jpg" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><strong>Kullabs School</strong> uploaded a note on Basic Definitions,  Time Domain Expressions For FM and PM</h4>
                                <p class="small text-muted">51 mins</p>
                            </div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s</p>
                            <div class="embed-responsive embed-responsive-4by3 thumbnail">
                                <iframe width="683" height="257" src="https://www.youtube.com/embed/N8nn13nxwRg" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <li class="status-message">
                                <div class="follow">
                                    <a href="#"><span class="fa fa-thumbs-up fa-fw"></span>Encourage</a>
                                    <a href="#"><span class="fa fa-comment fa-fw"></span>comment</a>
                                    <div class="dropdown share-option">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Share
                                            <span class="fa fa-share"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Reshare</a></li>
                                            <li><a href="#">Facebook</a></li>
                                            <li><a href="#">Twitter</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="images/female.jpg" width="28px" height="28px" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <form>
                                        <input type="text" class="form-control" placeholder="Add a comment..">
                                    </form>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="images/female.jpg" width="28px" height="28px" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body comment-list">
                                        <p class="media-heading">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                            Cras purus odio</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        <!-- Right-Sidebar-->
        <?php include_once('right-sidebar.php') ?>


</div>
</div>



<!-- Javascripts-->
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/plugins.js"></script>
<script src="js/exposureslider.js"></script>
<script src="js/main.js"></script>
</body>
</html>