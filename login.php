<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>KulLabs Pvt.</title>
  <!--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <!--<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
    <script src="js/jquery-2.1.4.min.js"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
<!--  <script src="js/index.js"></script>-->
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="row mg-btm">
      <div class="col-xs-6 col-md-8">
    <div class="login-head">
      <!--<img src="ronaldo.jpg" alt="profile_pic">-->
      <!--<img src="user.png" alt="profile_pic">-->
      <img src="images/user.png" alt="profile_pic">
    </div>
      </div>
      </div>
    <div class="form-signin mg-btm">
      <h2 class="heading-desc">Login to KulLabs</h2>

      <div class="main">
        <form id="loginForm" action="#" method="POST">
          <div class="login-form">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group has-primary has-feedback">
                <i class="fa fa-user"></i>
                <input type="text" class="form-control" placeholder="Enter Username " id="UserName">

              </div>
              <div class="form-group has-primary has-feedback">
                <div class="form-group log-status">
                  <i class="fa fa-lock"></i>
                  <input type="password" class="form-control" placeholder="Enter Password" id="Passwod">

                </div>
                <span class="alert">Invalid Credentials</span>
                <a class="link" href="#">Lost your password?</a>
                <button type="button" class="log-btn" >Log in</button>
              </div>
              <center> Don't have an account? <a href="signup-form.php"> Register for free</a></center>

              <span class="clearfix"></span>
              <div class="row omb_row-sm-offset-3 omb_loginOr">
                <div class="col-xs-12 col-sm-6 col-md-12">
                  <hr class="omb_hrOr">
                  <span class="omb_spanOr">OR</span>
                </div>
              </div>
              <div class="social-box">

                <div class="row">
                  <div class="input-group"  style="margin: -17px 0px 10px;">
					<span class="input-group-addon addon-facebook">
						<i class="fa fa-fw fa-2x fa-facebook fa-fw"></i>
					</span>
                    <a class="btn btn-lg btn-block btn-facebook" href="#"> Register with Facebook</a>
                  </div>

                  <div class="input-group">
					<span class="input-group-addon addon-googleplus">
						<i class="fa fa-fw fa-2x fa-google-plus  fa-fw"></i>
					</span>
                    <a class="btn btn-lg btn-block btn-googleplus" href="#"> Register with Google+</a>
                  </div>
              </div>
              </div>
            </div>
            <div class="panel-footer">

                <input type="submit" class="btn btn-success" id="goToChat" value="Sign Up Account" />


            </div>
          </div>
          </div>
        </form>

    </div>
  </div>
</div>
  </div>
</body>
</html>