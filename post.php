<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
        <div>
            <a href="post.html" class="create-btn"><span class="fa fa-plus"></span> create</a>
        </div>
    </div>
    <div class="row">
        <div class="post-detail">
            <div class="col-md-9">
                <span class="create-name">CREATE EXPOSURE</label></span>
                <form class=" well form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Exposure Name:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" placeholder=" Write Description here....."></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="">Category</label>
                        <div class="col-sm-10">
                            <select id="" name="" class="form-control">
                                <option value="1">Notes</option>
                                <option value="2">Videos</option>
                                <option value="3">Note</option>
                                <option value="4">Videos</option>
                                <option value="5">Videos</option>
                            </select>
                        </div>
                    </div>
                    <!-- Button (Double) -->
                    <div class="form-group" class="pull-right">
                        <label class="col-sm-2 control-label" for=""></label>
                        <div class="col-md-10">
                            <button class="btn btn-default">Close</button>
                            <button  class="btn btn-primary">Create Exposure</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!-- Right-Sidebar-->
        <?php include_once('right-sidebar.php') ?>
    </div>

</div>
</div>



<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/plugins.js"></script>
<script src="js/exposureslider.js"></script>
<script src="js/main.js"></script>
<script src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea' // change this value according to your HTML

    });
</script>
</body>
</html>