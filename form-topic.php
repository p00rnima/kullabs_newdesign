<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 c-container-div" style="background-color:#fff;margin-top: 10px;">
            <div>
                <h3 class="heading-topic">DISCUSSIONS ABOUT THIS NOTE</h3>
<!--                <a class="ask-question" aria-controls="ask-for-help" role="tab" data-target="#ask-question" data-toggle="modal">-->
<!--                    <i class="fa fa-question-circle" aria-hidden="true"></i> Ask For Question</a>-->
                <div style="float:right;margin-top:-38px;">
                    <button type="button" class="btn btn-default">View More</button>
                    <button type="button" class="btn btn-primary" aria-controls="ask-for-help" role="tab" data-target="#ask-question" data-toggle="modal"><i class="fa fa-question-circle" aria-hidden="true"></i> Ask For Question</button>
                    <!-- Modal -->
                </div>
                <div class="modal fade" id="ask-question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Ask Your Question To Your Module Leader</h4>
                            </div>
                            <div class="modal-body">
                                <div class="ask-for-help">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label for="firstName" class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control input-sm" type="text" id="firstName_student" placeholder="Enter Your Full Name" autofocus>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control input-sm" type="email" id="email-student" placeholder="Enter Your Email Address">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="col-sm-2 control-label">Question</label>
                                                    <div class="col-sm-10">
                                                        <textarea for="ask" class="form-control textarea-resize" id="ask-question-student" placeholder="Ask Your Question" rows="4"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- /form -->
                                        </div>
                                        <!-- end col-sm-9 col-md-12 -->
                                    </div>
                                    <!--end ROW -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Summit Your Question</button>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered table-hover">
                    <thead style="background-color:#4267b2">
                    <tr>
                        <th>Forum</th>
                        <th>Time</th>
                        <th>Replies</th>
                        <th>Report</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td style="width: 50%;">
                            <h5><img src="images/female.jpg" style="width:25px;"> Anju Gurung</h5>
                            <p>Define Gravity.</p>
                        </td>
                        <td class="forum-link" style="width: 25%;">
                            <p><small>Feb 05, 2017</small></p>
                            <!--                                <a class="note-comment-reply note-comment-reply-1998" data-id="1998" style="cursor: pointer;">Reply</a>-->
                        </td>
                        <td class="forum-link" style="width: 25%;">
                            <p class="count-replies-1998">0 Replies</p>
                            <a style="cursor: pointer;" class="viewAllReplies">View Replies</a>
                        </td>
                        <td> <span style="cursor: pointer;" title="Report this comment" class="pull-right report-comment-button report-comment-button-1998" data-id="1998" data-toggle="modal" data-target="#reportModal"><i class="fa fa-warning"></i></span></td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                            <h5><img src="images/female.jpg" style="width:25px;"> Yanjeen Rumpa</h5>
                            <p>why effect of gravitation is seen more in liquid than in solid?</p>
                        </td>
                        <td class="forum-link" style="width: 25%;">
                            <p><small>Feb 01, 2017</small></p>
                            <!--                                <a class="note-comment-reply note-comment-reply-1770" data-id="1770" style="cursor: pointer;">Reply</a>-->
                        </td>
                        <td class="forum-link" style="width: 25%;">
                            <p class="count-replies-1770">0 Replies</p>
                            <a style="cursor: pointer;" class="viewAllReplies">View Replies</a>
                        </td>
                        <td> <span style="cursor: pointer;" title="Report this comment" class="pull-right report-comment-button report-comment-button-1770" data-id="1770" data-toggle="modal" data-target="#reportModal"><i class="fa fa-warning"></i></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>