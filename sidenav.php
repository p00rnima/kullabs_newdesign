 <!-- Side-Nav-->
      <aside class="main-sidebar hidden-print">
        <section class="sidebar">
          <!--  <div class="user-panel">
            <div class="pull-left image"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image" class="img-circle"></div>
            <div class="pull-left info">
              <p>John Doe</p>
              <p class="designation">Frontend Developer</p>
            </div>
            </div> -->
          <!-- Sidebar Menu-->
          <ul class="sidebar-menu">
            <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-laptop"></i><span>UI Elements</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="bootstrap-componants.html"><i class="fa fa-circle-o"></i> Bootstrap Elements</a></li>
                <li><a href="ui-font-awesome.html"><i class="fa fa-circle-o"></i> Font Icons</a></li>
                <li><a href="ui-cards.html"><i class="fa fa-circle-o"></i> Cards</a></li>
                <li><a href="widgets.html"><i class="fa fa-circle-o"></i> Widgets</a></li>
              </ul>
            </li>
            <li><a href="charts.html"><i class="fa fa-pie-chart"></i><span>Charts</span></a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-edit"></i><span>Forms</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Form Componants</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Custom Componants</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Form Samples</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Form Notifications</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-th-list"></i><span>Tables</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Basic Tables</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Data Tables</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-file-text"></i><span>Pages</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> Blank Page</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Login Page</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> User Page</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Lockscreen Page</a></li>
                <li><a href=""><i class="fa fa-circle-o"></i> Error Page</a></li>
                
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-book"></i><span>MultiLavel Menu</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-book"></i> Level One</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-book"></i><span> Level One</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                 "><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li><a href="#"><i class="fa fa-book"></i><span> Level Two</span></a></li>
                  </ul>
                </li>
              </ul>
            </li>
              <li><a href="charts.html"><i class="fa fa-pie-chart"></i><span>Charts</span></a></li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-edit"></i><span>Forms</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Componants</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Custom Componants</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Samples</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Notifications</a></li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-th-list"></i><span>Tables</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Basic Tables</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Data Tables</a></li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-file-text"></i><span>Pages</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Blank Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Login Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> User Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Lockscreen Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Error Page</a></li>

                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-share"></i><span>MultiLavel Menu</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Level One</a></li>
                      <li class="treeview">
                          <a href="#"><i class="fa fa-circle-o"></i><span> Level One</span><i class="fa fa-angle-right"></i></a>
                          <ul class="treeview-menu">
                              "><i class="fa fa-circle-o"></i> Level Two</a></li>
                              <li><a href="#"><i class="fa fa-circle-o"></i><span> Level Two</span></a></li>
                          </ul>
                      </li>
                  </ul>
              </li>
              <li><a href="charts.html"><i class="fa fa-pie-chart"></i><span>Charts</span></a></li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-book"></i><span>Forms</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Componants</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Custom Componants</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Samples</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Form Notifications</a></li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-book"></i><span>Tables</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-book"></i> Basic Tables</a></li>
                      <li><a href=""><i class="fa fa-book"></i> Data Tables</a></li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-book"></i><span>Pages</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Blank Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Login Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> User Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Lockscreen Page</a></li>
                      <li><a href=""><i class="fa fa-circle-o"></i> Error Page</a></li>

                  </ul>
              </li>
              <li class="treeview">
                  <a href="#"><i class="fa fa-book"></i><span>MultiLavel Menu</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href=""><i class="fa fa-circle-o"></i> Level One</a></li>
                      <li class="treeview">
                          <a href="#"><i class="fa fa-circle-o"></i><span> Level One</span><i class="fa fa-angle-right"></i></a>
                          <ul class="treeview-menu">
                              "><i class="fa fa-circle-o"></i> Level Two</a></li>
                              <li><a href="#"><i class="fa fa-circle-o"></i><span> Level Two</span></a></li>
                          </ul>
                      </li>
                  </ul>
              </li>

          </ul>
        </section>
      </aside>