<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Exposure</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="card" style="margin-top: 6px;">
                <div class="lesson_pts">
                    <a href="" class="btn btn-sm btn-primary">
                        <i class="glyphicon glyphicon-ok-circle"></i> Practice test #1
                    </a>
                    <a href="" class="btn btn-sm btn-primary">
                        <i class="glyphicon glyphicon-ok-circle"></i> Practice test #2
                    </a>
                    <a href="" class="btn btn-sm btn-primary">
                        <i class="glyphicon glyphicon-ok-circle"></i> Practice test #3
                    </a>
                    <a href="" class="btn btn-sm btn-primary">
                        <i class="glyphicon glyphicon-ok-circle"></i> Practice test #3
                    </a>
                    <a href="" class="btn btn-sm btn-primary">
                        <i class="glyphicon glyphicon-ok-circle"></i> Practice test #3
                    </a>
                </div>
                <h3>Practice Session for each note on this lesson</h3>
                <table class="table table-striped table-hover">
                    <tbody class="_practicebody">
                    <tr>
                        <td>
                            <a href="">
                                <span class="fa fa-angle-double-right"></span>   Measurement</a>
                        </td>
                        <td style="text-align:right"><a href=""><i class="glyphicon glyphicon-ok-circle"></i> Take Practice Test</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="">
                                <span class="fa fa-angle-double-right"></span>   Measurement</a>
                        </td>
                        <td style="text-align:right"><a href=""><i class="glyphicon glyphicon-ok-circle"></i> Take Practice Test</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="">
                                <span class="fa fa-angle-double-right"></span>  Measurement of Some Fundamental and Derived Quantities</a>
                        </td>
                        <td style="text-align:right"><a href=""><i class="glyphicon glyphicon-ok-circle"></i> Take Practice Test</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>