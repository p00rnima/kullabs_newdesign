<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Unit-page</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="lesson-page">
            <div class="col-md-9">
                <!-- START NIJAN // UNIT -->
                <div class="main-content">
                    <!-- accordion -->
                    <div class="accordion">
                        <h4 class="subject-head">Physics Lessons</h4>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <ul class="hidden-xs panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>
                                            <li><a href="#">27 Exercises</a></li>
                                            <li><a href="#">3 Practise Test</a></li>
                                        </ul>
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1-1" aria-expanded="true" aria-controls="collapseOne"><p class="panel-title-bold"> Units and Measurement </p></a> </h4>
                                </div>
                                <div id="collapse1-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <ul class="visible-xs panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>

                                        </ul>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/measurement.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#"> Measurement</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/mass.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Mass</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/time.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement of Time</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/img-04.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement Units</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <ul class="hidden-xs  panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>
                                            <li><a href="#">27 Exercises</a></li>
                                            <li><a href="#">3 Practise Test</a></li>
                                        </ul>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1-2" aria-expanded="false" aria-controls="collapseTwo"><p class="panel-title-bold"> Velocity and Acceleration</p> </a> </h4>
                                </div>
                                <div id="collapse1-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <ul class="visible-xs panel-tab">
                                                    <li><a href="#">2 Notes</a></li>
                                                    <li><a href="#">6 Videos</a></li>
                                                    <li><a href="#">27 Exercises</a></li>
                                                    <li><a href="#">3 Practise Test</a></li>
                                                </ul>
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/measurement.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#"> Measurement</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/mass.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Mass</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/time.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement of Time</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/img-04.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement Units</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <ul class="hidden-xs  panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>
                                            <li><a href="#">27 Exercises</a></li>
                                            <li><a href="#">3 Practise Test</a></li>
                                        </ul>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1-3" aria-expanded="false" aria-controls="collapseThree"><p class="panel-title-bold"> Simple Machine</p> </a> </h4>
                                </div>
                                <div id="collapse1-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/measurement.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#"> Measurement</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 panel-tab">
                                                <ul class="visible-xs">
                                                    <li><a href="#">2 Notes</a></li>
                                                    <li><a href="#">6 Videos</a></li>
                                                    <li><a href="#">27 Exercises</a></li>
                                                    <li><a href="#">3 Practise Test</a></li>
                                                </ul>
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/mass.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Mass</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/time.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement of Time</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/img-04.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement Units</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <ul class="hidden-xs  panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>
                                            <li><a href="#">27 Exercises</a></li>
                                            <li><a href="#">3 Practise Test</a></li>
                                        </ul>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1-4" aria-expanded="false" aria-controls="collapseFour"> <p class="panel-title-bold">Pressure </p></a> </h4>
                                </div>
                                <div id="collapse1-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <ul class="visible-xs panel-tab">
                                                    <li><a href="#">2 Notes</a></li>
                                                    <li><a href="#">6 Videos</a></li>
                                                    <li><a href="#">27 Exercises</a></li>
                                                    <li><a href="#">3 Practise Test</a></li>
                                                </ul>
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/measurement.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#"> Measurement</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/mass.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Mass</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/time.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement of Time</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/img-04.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement Units</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                        <ul class="hidden-xs panel-tab">
                                            <li><a href="#">2 Notes</a></li>
                                            <li><a href="#">6 Videos</a></li>
                                            <li><a href="#">27 Exercises</a></li>
                                            <li><a href="#">3 Practise Test</a></li>
                                        </ul>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1-5" aria-expanded="false" aria-controls="collapseFive"><p class="panel-title-bold"> Work, Energy and Power</p> </a> </h4>
                                </div>
                                <div id="collapse1-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <ul class="visible-xs panel-tab">
                                                    <li><a href="#">2 Notes</a></li>
                                                    <li><a href="#">6 Videos</a></li>
                                                    <li><a href="#">27 Exercises</a></li>
                                                    <li><a href="#">3 Practise Test</a></li>
                                                </ul>
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/measurement.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#"> Measurement</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/mass.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Mass</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/time.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement of Time</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="thumbnail" > <a href="#"><img alt="Measurement" title="Measurement" src="images/img-04.jpg"></a>
                                                    <div class="caption">
                                                        <p><a href="#">Measurement Units</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>