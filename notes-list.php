<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">class list</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <div class="class_list">
            <div class="col-md-9">
                <ul class="gallery-items">
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="images/e-learning/class7.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">
                                        Class : 7 Notes
                                        <span class="title_detail">
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <p>Notes : Lorem Ipsum has been the industry's standard dummy text</p>
                              </span>
                                    </div>
                                    <div class="contents">
                                        <p>Relates notes:</p>
                                        <div id="demo1" class="flex-images">
                                            <div class="item" data-w="219" data-h="180" style="background-color:#2ecc71">
                                                <div class="num_notelist">
                                                    <h4> Videos : 8 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="279" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Notes : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="270" data-h="180" style="background-color:#fd881c;">
                                                <div class="num_notelist">
                                                    <h4> Exercises : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="283" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Syllabus : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="271" data-h="180" style="background-color:#0e76bc;">
                                                <div class="num_notelist">
                                                    <h4>News : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="258" data-h="180" style="background-color:#34495e;">
                                                <div class="num_notelist">
                                                    <h4>Users : 20 </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="images/e-learning/class7.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">
                                        Class : 7 Notes
                                        <span class="title_detail">
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <p>Notes : Lorem Ipsum has been the industry's standard dummy text</p>
                              </span>
                                    </div>
                                    <div class="contents">
                                        <p>Relates notes:</p>
                                        <div id="demo1" class="flex-images">
                                            <div class="item" data-w="219" data-h="180" style="background-color:#2ecc71">
                                                <div class="num_notelist">
                                                    <h4> Videos : 8 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="279" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Notes : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="270" data-h="180" style="background-color:#fd881c;">
                                                <div class="num_notelist">
                                                    <h4> Exercises : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="283" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Syllabus : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="271" data-h="180" style="background-color:#0e76bc;">
                                                <div class="num_notelist">
                                                    <h4>News : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="258" data-h="180" style="background-color:#34495e;">
                                                <div class="num_notelist">
                                                    <h4>Users : 20 </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="images/e-learning/class7.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">
                                        Class : 7 Notes
                                        <span class="title_detail">
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <p>Notes : Lorem Ipsum has been the industry's standard dummy text</p>
                              </span>
                                    </div>
                                    <div class="contents">
                                        <p>Relates notes:</p>
                                        <div id="demo1" class="flex-images">
                                            <div class="item" data-w="219" data-h="180" style="background-color:#2ecc71">
                                                <div class="num_notelist">
                                                    <h4> Videos : 8 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="279" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Notes : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="270" data-h="180" style="background-color:#fd881c;">
                                                <div class="num_notelist">
                                                    <h4> Exercises : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="283" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Syllabus : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="271" data-h="180" style="background-color:#0e76bc;">
                                                <div class="num_notelist">
                                                    <h4>News : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="258" data-h="180" style="background-color:#34495e;">
                                                <div class="num_notelist">
                                                    <h4>Users : 20 </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="images/e-learning/class7.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">
                                        Class : 8 Notes
                                        <span class="title_detail">
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <p>Notes : Lorem Ipsum has been the industry's standard dummy text</p>
                              </span>
                                    </div>
                                    <div class="contents">
                                        <p>Relates notes:</p>
                                        <div id="demo1" class="flex-images">
                                            <div class="item" data-w="219" data-h="180"><img src="images/1.jpg"></div>
                                            <div class="item" data-w="279" data-h="180"><img src="images/2.jpg"></div>
                                            <div class="item" data-w="270" data-h="180"><img src="images/12.jpg"></div>
                                            <div class="item" data-w="283" data-h="180"><img src="images/13.jpg"></div>
                                            <div class="item" data-w="271" data-h="180"><img src="images/14.jpg"></div>
                                            <div class="item" data-w="258" data-h="180"><img src="images/15.jpg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="example/4.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">Gallery Item</div>
                                    <div class="contents"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="gallery-item">
                        <div class="gallery-contents">
                            <div class="thumbnail gallery-trigger">
                                <div class="feature-note">
                                    <i class="fa fa-book"></i>
                                </div>
                                <ul>
                                    <li><a href="#">No. of notes:24</a></li>
                                    <li><a href="#">No. of MCQ:24</a></li>
                                    <li><a href="#">No. of questions:24</a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="gallery-expander">
                            <div class="gallery-expander-contents">
                                <div class="gallery-trigger-close close">x</div>
                                <div class="col">
                                    <div class="image"><img src="images/e-learning/class7.jpg" alt="" /></div>
                                </div>
                                <div class="col">
                                    <div class="title">
                                        Class : 7 Notes
                                        <span class="title_detail">
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <p>Notes : Lorem Ipsum has been the industry's standard dummy text</p>
                              </span>
                                    </div>
                                    <div class="contents">
                                        <p>Relates notes:</p>
                                        <div id="demo1" class="flex-images">
                                            <div class="item" data-w="219" data-h="180" style="background-color:#2ecc71">
                                                <div class="num_notelist">
                                                    <h4> Videos : 8 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="279" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Notes : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="270" data-h="180" style="background-color:#fd881c;">
                                                <div class="num_notelist">
                                                    <h4> Exercises : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="283" data-h="180" style="background-color:#000;">
                                                <div class="num_notelist">
                                                    <h4> Syllabus : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="271" data-h="180" style="background-color:#0e76bc;">
                                                <div class="num_notelist">
                                                    <h4>News : 20 </h4>
                                                </div>
                                            </div>
                                            <div class="item" data-w="258" data-h="180" style="background-color:#34495e;">
                                                <div class="num_notelist">
                                                    <h4>Users : 20 </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>



                </ul>
            </div>
        </div>
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>

</div>
<!-- Javascripts-->
<!--<script src="js/jquery-2.1.4.min.js"></script>-->
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- jQuery (necessary for google style expanding image JavaScript plugins) -->
<script src="js/jquery.imagelistexpander.js"></script>
<!-- jQuery (necessary for flex-images JavaScript plugins) -->
<script src="js/flex-images.js"></script>
<script src="js/main.js"></script>

<script>
    (function(global, $){
        $('.gallery-items').imagelistexpander({
            prefix: "gallery-"
        });
    })(this, jQuery)
</script>
<script>
    new flexImages({selector: '#demo1', rowHeight: 140});
    if (~window.location.href.indexOf('http')) {
        (function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;po.src = 'https://apis.google.com/js/plusone.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);})();
        (function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=114593902037957";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
        document.getElementById('github_social').innerHTML = '\
              <iframe style="float:left;margin-right:15px" src="//ghbtns.com/github-btn.html?user=Pixabay&repo=JavaScript-flexImages&type=watch&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="110" height="20"></iframe>\
              <iframe style="float:left;margin-right:15px" src="//ghbtns.com/github-btn.html?user=Pixabay&repo=JavaScript-flexImages&type=fork&count=true" allowtransparency="true" frameborder="0" scrolling="0" width="110" height="20"></iframe>\
          ';
    }
</script>

</body>
</html>