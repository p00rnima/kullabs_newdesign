<div class="col-md-3">
    <!--    div for sponsored-->
    <div class="sidebar_modules" style="margin-top: 6px;">
        <div class="panel panel-default">
            <div class="sponsor_rgt">
                <img src="images/studentpage/sponsor.png" style="width:100%">
                <h2>SPONSORED </h2>
                <hr class="sidebar-separation-line">
            </div>
            <div class="sponsor_rgt">
                <img src="images/studentpage/sponsor1.png" style="width:100%">
                <p>Are you student truned entrepreneur</p>
                <a href="">
                    <p>www.example.com</p>
                </a>
                <h4 class="question_topic">Do you have Pets?</h4>
                <form>
                    <input type="radio" value="female" name="gender"> Yes, a cat<br>
                    <input type="radio" value="female" name="gender"> Yes, a dog<br>
                    <input type="radio" value="female" name="gender"> Yes, another kind<br>
                    <input type="radio" value="female" name="gender"> Yes, more than one<br>
                    <input type="radio" value="female" name="gender"> No<br>
                </form>
            </div>
        </div>
    </div>
    <!--    div for sponsored end-->

    <!-- div for ads -->
    <div class="span3">
        <a href="https://kullabs.com/contact-us">
        </a>
        <div class="feature-box"><a href="https://kullabs.com/contact-us">
                <img class="feature-box-image" alt="" src="https://kullabs.com/img/home/99.jpg">
                <h4 class="feature-box-title">Distar 32'' with Three Apps</h4>
                <ul>
                    <li>1400 notes</li>
                    <li>2900+ videos</li>
                    <li>1300+ Solved Exercises</li>
                    <li>1300+ Quizzes</li>
                </ul>
            </a><a class="btn btn-primary btn-large btn-block" href="https://kullabs.com/contact-us">Contact Us</a>
        </div>
    </div>
    <!-- div for ads end -->

    <!--    div for related note-->
    <div class="side-bar">
        <div class="note">
            <div class="list-head">
                <span class="fa fa-book"></span>
                <h5>Related Notes</h5>
            </div>
            <div class="side-note">
                <!-- note block -->
                <div class="note-block">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="images/mass.jpg" alt="..." style="width:75px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading">Regional and Federal Development </h5>
                        </div>
                        <p>  Grade 10 » Social Studies » We Our Community And Nation</p>
                        <a href="" class="read-note pull-right">
                            Read Note »
                        </a>
                    </div>
                </div>
                <!-- note block -->
                <div class="note-block">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="images/time.jpg" alt="..." style="width:75px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading">Measurement of Time</h5>
                        </div>
                        <p>  Grade 10 » Social Studies » We Our Community And Nation</p>
                        <a href="" class="read-note pull-right">
                            Read Note »
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    div for related note end-->
</div>