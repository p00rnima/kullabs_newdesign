<!-- header  -->
<?php include_once('header.php') ?>
<!-- Side-Nav-->
<?php include_once('sidenav.php') ?>


<div class="content-wrapper">
    <div class="page-title">
        <!--  <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p> admin template</p>
          </div> -->
        <div>
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li class="active">Forum-panel</li>
            </ul>
        </div>
<!--        <div>-->
<!--            <a href="post.php" class="create-btn"><span class="fa fa-plus"></span> create</a>-->
<!--        </div>-->
    </div>
    <div class="row">
        <!-- start LESSON-PAGE -->
        <div class="forum-topic lesson-page">
            <div class="col-md-9">
                <!------- start  TEACHER-PANEL   ---------->
                <div class ="well well-lg">
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#"><img class="media-object" src="images/e-learning/teacher.png" alt="teacher-panel-icon"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading forum-title">Teacher Forum Panel</h4>
                            <!-- start TAB-SECTION -->
                            <div class="tab-section">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#lesson-plan" aria-controls="lesson-plan" role="tab" data-toggle="tab"><i class="fa fa-file-text" aria-hidden="true"></i>Lesson Plan</a></li>
                                    <li role="presentation"><a href="#discussion" aria-controls="discussion" role="tab" data-toggle="tab"><i class="fa fa-area-chart" aria-hidden="true"></i>Discusssion</a></li>
                                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-area-chart" aria-hidden="true"></i>Analytical report</a></li>
                                    <li role="presentation"><a href="#ask-for-help" aria-controls="ask-for-help" role="tab" data-toggle="tab"><i class="fa fa-question-circle" aria-hidden="true"></i>Ask for help</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!--start LESSON-PLAN tab -->
                                    <div role="tabpanel" class="tab-pane" id="lesson-plan">
                                        <div class="forum-content">
                                            <!-- forum -->
                                            <div class="features">
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Class 8</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Any Question ?</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa  fa-users"></i>
                                                        <h2>Discussion</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Adipisicing elit</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Sed do eiusmod</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-users"></i>
                                                        <h2>Labore et dolore</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                            </div>
                                            <!-- end Features -->
                                        </div>
                                    </div>
                                    <!--start LESSON-PLAN tab -->
                                    <!--start DISCUSSION tab -->
                                    <div role="tabpanel" class="tab-pane" id="discussion">
                                        <div class="row">
                                            <div class="col-sm-9 col-md-12">
                                                <div class="msg-wrap">
                                                    <a href="#" class="">
                                                        <div class="msg odd">
                                                            <div class="col-md-3 author">
                                                                <h5 class="media-heading">Shuva Raj Sharma <small class="text-muted"> </small></h5>
                                                                <small class="text-muted">
                                                                    <i class="fa fa-clock-o"></i> 07:30am</small>
                                                                <img src="images/e-learning/user-1.png" alt="user 1" data-src="holder.js/64x64" class="media-object">
                                                            </div>
                                                            <div class="col-md-9">
                                                                Shuva Raj Sharma: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="">
                                                        <div class="msg even">
                                                            <div class="col-md-3 author">
                                                                <h5 class="media-heading">Purnima Gurung<small class="text-muted"> </small></h5>
                                                                <small class="text-muted">
                                                                    <i class="fa fa-clock-o"></i> 12:10am</small>
                                                                <img src="images/e-learning/user-2.png" data-src="holder.js/64x64" class="media-object">
                                                            </div>
                                                            <div class="col-md-9">
                                                                Purnima Gurung: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="">
                                                        <div class="msg odd">
                                                            <div class="col-md-3 author">
                                                                <h5 class="media-heading">Nijan Singh Thakuri <small class="text-muted"> </small></h5>
                                                                <small class="text-muted">
                                                                    <i class="fa fa-clock-o"></i> 16:40pm</small>
                                                                <img src="images/e-learning/user-3.png" alt="user 3" data-src="holder.js/64x64" class="media-object">
                                                            </div>
                                                            <div class="col-md-9">
                                                                Nijan Singh Thakuri: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="">
                                                        <div class="msg even">
                                                            <div class="col-md-3 author">
                                                                <h5 class="media-heading">Shailesh Limbu <small class="text-muted"> </small></h5>
                                                                <small class="text-muted">
                                                                    <i class="fa fa-clock-o"></i> 22:20am</small>
                                                                <img src="images/e-learning/user-4.png" alt="user 4" data-src="holder.js/64x64" class="media-object">
                                                            </div>
                                                            <div class="col-md-9">
                                                                Shailesh Limbu: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="send-wrap ">
                                                    <form accept-charset="UTF-8" action="" method="POST" role="form" class="">
                                                        <div class="form-group">
                                                            <textarea class="form-control counted textarea-resize" rows="3" placeholder="Write a reply..."></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <h6 class="pull-right">2500 characters remaining</h6>
                                                            <button class="btn btn-info btn-md" type="submit">Send</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end DISCUSSION tab -->
                                    <!--start REPORT tab -->
                                    <div role="tabpanel" class="tab-pane" id="messages">
                                        <div class="row">
                                            <div class="report">
                                                <div role="tabpanel" class="tab-pane" id="ask-for-help">
                                                <h2> This section is underconstruction</h2>
                                                <img src="images/e-learning/page-under-construction.jpg" class="img-responsive" alt="Page Under Construction">
                                            </div>
                                        </div>
                                    </div>
                                    <!--end REPORT tab -->
                                    <!--start ASK FOR HELP tab -->
                                    <div role="tabpanel" class="tab-pane" id="ask-for-help">
                                        <div class="ask-for-help">
                                            <div class="row">
                                                <div class="col-xs-9 col-sm-9 col-md-12">
                                                    <form class="form-horizontal" role="form">
                                                        <h2> Ask Your Question To Your Module Leader</h2>
                                                        <div class="form-group">
                                                            <label for="firstName" class="col-sm-3 control-label">Name</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control input-sm" type="text" id="firstName_student" placeholder="Enter Your Full Name" autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control input-sm" type="email" id="email-student" placeholder="Enter Your Email Address">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="question" class="col-sm-3 control-label">Question</label>
                                                            <div class="col-sm-6">
                                                                <textarea for="ask" class="form-control textarea-resize" id="ask-question-student" placeholder="Ask Your Question" rows="4"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6 col-sm-offset-3">
                                                                <button type="submit" class="btn btn-primary btn-block">Submit Your Question</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- /form -->
                                                </div>
                                                <!-- end col-sm-9 col-md-12 -->
                                            </div>
                                            <!--end ROW -->
                                        </div>
                                    </div>
                                    <!-- end ASK FOR HELP -->
                                </div>
                                <!-- end TAB-CONTENT -->
                            </div>
                            <!-- end TAB-SECTION -->
                        </div>
                        <!-- end media-body -->
                    </div>
                    <!-- end media -->
                </div>
                <!-- end well-lg -->
                <!--  end TEACHER-PANEL -------->
                <!--  start STUDENT-PANEL -------->
                <div class ="well well-lg">
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#"><img class="media-object" src="images/e-learning/student.png" alt="teacher-panel-icon"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading forum-title">Student Forum Panel</h4>
                            <!-- start TAB-SECTION -->
                            <div class="tab-section">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#home2" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-file-text" aria-hidden="true"></i>Lesson Plan</a></li>
                                    <li role="presentation"><a href="#discussion2" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-area-chart" aria-hidden="true"></i>Discusssion</a></li>
                                    <li role="presentation"><a href="#messages2" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-area-chart" aria-hidden="true"></i>Analytical report</a></li>
                                    <li role="presentation"><a href="#ask-for-help2" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-question-circle" aria-hidden="true"></i>Ask for help</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="home2">
                                        <div class="forum-content">
                                            <!-- forum -->
                                            <div class="features">
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Class 8</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Any Question ?</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa  fa-users"></i>
                                                        <h2>Discussion</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Adipisicing elit</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Sed do eiusmod</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-users"></i>
                                                        <h2>Labore et dolore</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                            </div>
                                            <!-- end Features -->
                                        </div>
                                    </div>
                                    <!--start DISCUSSION tab -->
                                    <div role="tabpanel" class="tab-pane" id="discussion2">
                                        <div class="row">
                                            <div class="col-sm-9 col-md-12">
                                                <div class="msg-wrap">
                                                    <div class="msg odd">
                                                        <div class="col-md-3 author">
                                                            <h5 class="media-heading">Shuva Raj Sharma <small class="text-muted"> </small></h5>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock-o"></i> 07:30am</small>
                                                            <a href="#" class="">
                                                                <img src="images/e-learning/user-1.png" alt="user 1" data-src="holder.js/64x64" class="media-object">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-9">
                                                            Shuva Raj Sharma: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="msg even">
                                                        <div class="col-md-3 author">
                                                            <h5 class="media-heading">Purnima Gurung<small class="text-muted"> </small></h5>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock-o"></i> 12:10am</small>
                                                            <a href="#" class="">
                                                                <img src="images/e-learning/user-2.png" data-src="holder.js/64x64" class="media-object">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-9">
                                                            Purnima Gurung: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="msg odd">
                                                        <div class="col-md-3 author">
                                                            <h5 class="media-heading">Nijan Singh Thakuri <small class="text-muted"> </small></h5>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock-o"></i> 16:40pm</small>
                                                            <a href="#" class="">
                                                                <img src="images/e-learning/user-3.png" alt="user 3" data-src="holder.js/64x64" class="media-object">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-9">
                                                            Nijan Singh Thakuri: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="msg even">
                                                        <div class="col-md-3 author">
                                                            <h5 class="media-heading">Shailesh Limbu <small class="text-muted"> </small></h5>
                                                            <small class="text-muted">
                                                                <i class="fa fa-clock-o"></i> 22:20am</small>
                                                            <a href="#" class="">
                                                                <img src="images/e-learning/user-4.png" alt="user 4" data-src="holder.js/64x64" class="media-object">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-9">
                                                            Shailesh Limbu: "Some people close to Congress Party and close to the government had a #secret #meeting in a farmhouse in Maharashtra in which Anna Hazare send some representatives and they had a meeting in the discussed how to go about this all fast and how eventually this will end."
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="send-wrap ">
                                                    <form accept-charset="UTF-8" action="" method="POST" role="form" class="">
                                                        <div class="form-group">
                                                            <textarea class="form-control counted textarea-resize" rows="3" placeholder="Write a reply..."></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <h6 class="pull-right" id="counter">2500 characters remaining</h6>
                                                            <button class="btn btn-info btn-md" type="submit">Send</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end DISCUSSION tab -->
                                    <!--start REPORT tab -->
                                    <div role="tabpanel" class="tab-pane" id="messages2">
                                        <div class="row">
                                            <div class="report">
                                                <h2> This section is underconstruction</h2>
                                                <img src="images/e-learning/page-under-construction.jpg" class="img-responsive" alt="Page Under Construction">
                                            </div>
                                        </div>
                                    </div>
                                    <!--end REPORT tab -->
                                    <!--start ASK FOR HELP tab -->
                                    <div role="tabpanel" class="tab-pane" id="ask-for-help2">
                                        <div class="ask-for-help">
                                            <div class="row">
                                                <div class="col-xs-9 col-sm-9 col-md-12">
                                                    <form class="form-horizontal" role="form">
                                                        <h2> Ask Your Question To Your Module Leader</h2>
                                                        <div class="form-group">
                                                            <label for="firstName" class="col-sm-3 control-label">Name</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control input-sm" type="text" id="firstName" placeholder="Enter Your Full Name" autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control input-sm" type="email" id="email" placeholder="Enter Your Email Address">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="question" class="col-sm-3 control-label">Question</label>
                                                            <div class="col-sm-6">
                                                                <textarea for="ask" class="form-control textarea-resize" id="ask-question" placeholder="Ask Your Question" rows="4"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6 col-sm-offset-3">
                                                                <button type="submit" class="btn btn-primary btn-block">Submit Your Question</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- /form -->
                                                </div>
                                                <!--end col-xs-9 col-sm-9 col-md-12 -->
                                            </div>
                                            <!--end ROW -->
                                        </div>
                                    </div>
                                    <!-- end ASK FOR HELP -->
                                </div>
                                <!-- end TAB-CONTENT -->
                            </div>
                            <!-- end TAB-SECTION -->
                        </div>
                    </div>
                </div>
                <!-- end well-lg -->
                <!--  end -------STUDENT-PANEL------     -->
                <!--  start -------PARENT-PANEL------     -->
                <div class ="well well-lg">
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#"><img class="media-object" src="images/e-learning/parent.png" alt="teacher-panel-icon"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading forum-title">Parent Forum Panel</h4>
                            <!-- start TAB-SECTION -->
                            <div class="tab-section">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#home3" aria-controls="home" role="tab" data-toggle="tab"><span><i class="fa fa-file-text" aria-hidden="true"></i></span>Lesson Plan</a></li>
                                    <li role="presentation"><a href="#profile3" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-comment" aria-hidden="true"></i>Discusssion</a></li>
                                    <li role="presentation"><a href="#messages3" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-area-chart" aria-hidden="true"></i>Analytical report</a></li>
                                    <li role="presentation"><a href="#settings3" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-question-circle" aria-hidden="true"></i>Ask for help</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="home3">
                                        <div class="forum-content">
                                            <!-- forum -->
                                            <div class="features">
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Class 8</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Any Question ?</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa  fa-users"></i>
                                                        <h2>Discussion</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-book"></i>
                                                        <h2>Adipisicing elit</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-question-circle"></i>
                                                        <h2>Sed do eiusmod</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="feature-wrap">
                                                        <i class="fa fa-users"></i>
                                                        <h2>Labore et dolore</h2>
                                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                                                    </div>
                                                </div>
                                                <!--/.col-md-4-->
                                            </div>
                                            <!-- end Features -->
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile3">...</div>
                                    <div role="tabpanel" class="tab-pane" id="messages3">....</div>
                                    <div role="tabpanel" class="tab-pane" id="settings3">....</div>
                                </div>
                                <!-- end TAB-CONTENT -->
                            </div>
                            <!-- end TAB-SECTION -->
                        </div>
                    </div>
                </div>
                <!-- end well-lg -->
                <!--  end ------- PARENT-PANEL ------     -->
            </div>
            <!-- end COL-MD-9 -->
        </div>
        <!-- end LESSON-PAGE -->
        <?php include_once ('right-sidebar.php') ?>
    </div>
</div>
</div>
<!-- Javascripts-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/essential-plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/pace.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>